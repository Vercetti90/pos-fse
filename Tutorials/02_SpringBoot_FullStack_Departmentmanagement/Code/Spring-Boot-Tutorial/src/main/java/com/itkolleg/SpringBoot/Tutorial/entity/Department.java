package com.itkolleg.SpringBoot.Tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;

//Important Annotation to make this class an entity and to interact with the database
@Entity
//Import Annotation for Lombok
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Department {

    //Important for setting the primary key
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    //Datenfelder / Properties
    private Long departmentId;

    /**
     * Different Validation Types
     *
     * @Length(max = 10,min = 0)
     * @Size(max = 10, min = 0)
     * @Email
     * @Positive
     * @Negative
     * @PositiveOrZero
     * @NegativeOrZero
     * @Future
     * @FutureOrPresent
     * @Past
     * @PastOrPresent
     */
    @NotBlank(message = "Please Add Department Name!")
    private String departmentName;
    private String departmentAddress;
    private String departmentCode;



}

//    //Constructors
//    public Department() {
//    }
//
//    public Department(Long departmentId, String departmentName, String departmentAddress, String departmentCode) {
//        this.departmentId = departmentId;
//        this.departmentName = departmentName;
//        this.departmentAddress = departmentAddress;
//        this.departmentCode = departmentCode;
//    }
//    //toString-Method
//    @Override
//    public String toString() {
//        return "Department{" +
//                "departmentId=" + departmentId +
//                ", departmentName='" + departmentName + '\'' +
//                ", departmentAddress='" + departmentAddress + '\'' +
//                ", departmentCode='" + departmentCode + '\'' +
//                '}';
//    }
//
//    //Getter & Setter
//    public Long getDepartmentId() {
//        return departmentId;
//    }
//
//    public void setDepartmentId(Long departmentId) {
//        this.departmentId = departmentId;
//    }
//
//    public String getDepartmentName() {
//        return departmentName;
//    }
//
//    public void setDepartmentName(String departmentName) {
//        this.departmentName = departmentName;
//    }
//
//    public String getDepartmentAddress() {
//        return departmentAddress;
//    }
//
//    public void setDepartmentAddress(String departmentAddress) {
//        this.departmentAddress = departmentAddress;
//    }
//
//    public String getDepartmentCode() {
//        return departmentCode;
//    }
//
//    public void setDepartmentCode(String departmentCode) {
//        this.departmentCode = departmentCode;
//    }

