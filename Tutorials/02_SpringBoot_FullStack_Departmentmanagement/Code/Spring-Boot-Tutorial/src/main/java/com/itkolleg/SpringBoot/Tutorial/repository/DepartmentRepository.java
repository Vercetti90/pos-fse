package com.itkolleg.SpringBoot.Tutorial.repository;

import com.itkolleg.SpringBoot.Tutorial.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository

public interface DepartmentRepository  extends JpaRepository<Department, Long> {

    public Department findByDepartmentName(String departmentName);

    //This is how we can implement the different JPA-Methods
    //We use the IgnoreCase Keyword to activate Case Insensitive
    public Department findByDepartmentNameIgnoreCase(String departmentName);
}
