package com.example.springbootsecondentity.service;

import com.example.springbootsecondentity.entity.Department;
import com.example.springbootsecondentity.entity.Employee;
import com.example.springbootsecondentity.repository.DepartmentRepository;
import com.example.springbootsecondentity.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;
    @MockBean
    private EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        Employee employee = Employee.builder()
                .employeeFirstName("Thomas")
                .employeeLastName("Kutz")
                .employeeId(1L)
                .build();

        Mockito.when(employeeRepository.findByEmployeeLastNameIgnoreCase("Kutz"))
                .thenReturn(employee);
    }

    @Test
    @DisplayName("Get Data based Valid Employee Name")
    public void whenValidEmployeeName_thenEmployeeShouldFound(){
        String employeeLastName = "Kutz";
        Employee found = employeeService.fetchEmployeeByLastName(employeeLastName);
        assertEquals(employeeLastName, found.getEmployeeLastName());
    }
}
