package com.example.springbootsecondentity.controller;

import com.example.springbootsecondentity.entity.Department;
import com.example.springbootsecondentity.entity.Employee;
import com.example.springbootsecondentity.service.DepartmentService;
import com.example.springbootsecondentity.service.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EmployeeService employeeService;
    private Employee employee;

    @BeforeEach
    void setUp(){
        employee = Employee.builder()
                .employeeFirstName("Thomas")
                .employeeLastName("Kutz")
                .employeeId(1L)
                .build();
    }
    @Test
    void saveEmployee() throws Exception {
        Employee inputEmployee = Employee.builder()
                .employeeFirstName("Thomas")
                .employeeLastName("Kutz")
                .build();

        Mockito.when(employeeService.saveEmployee(inputEmployee))
                .thenReturn(employee);

        //POST-Operation
        mockMvc.perform(post("/employee")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"employeeFirstName\":\"Thomas\",\n" +
                                "\t\"employeeLastName\":\"Kutz\",\n" +
                                "}"))
                .andExpect(status().isOk());
    }
    @Test
    void fetchEmployeeById() throws Exception {
        Mockito.when(employeeService.fetchEmployeeById(1L))
                .thenReturn(employee);
        //GET-Operation
        mockMvc.perform(get("/employee/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.employeeLastName").value(employee.getEmployeeLastName()));
    }
}
