package com.example.springbootsecondentity.repository;

import com.example.springbootsecondentity.entity.Department;
import com.example.springbootsecondentity.entity.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
public class EmployeeRepositoryTest {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void setUp() {
        Employee employee = Employee.builder()
                .employeeFirstName("Thomas")
                .employeeLastName("Kutz")
                .build();

        entityManager.persist(employee);
    }
    @Test
    public void whenFindById_thenReturnEmployee(){
        Employee employee = employeeRepository.findById(1L).get();
        assertEquals(employee.getEmployeeLastName(), "Kutz");
    }
}
