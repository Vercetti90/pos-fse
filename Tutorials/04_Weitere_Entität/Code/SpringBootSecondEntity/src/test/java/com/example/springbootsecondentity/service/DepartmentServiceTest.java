package com.example.springbootsecondentity.service;

import com.example.springbootsecondentity.entity.Department;
import com.example.springbootsecondentity.repository.DepartmentRepository;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DepartmentServiceTest {

    @Autowired
    private DepartmentService departmentService;

    //Annotation for Mocking
    @MockBean
    private DepartmentRepository departmentRepository;

    @BeforeEach
        //@BeforeAll
    void setUp() {
        Department department = Department.builder()
                .departmentName("IT")
                .departmentAddress("Innsbruck")
                .departmentCode("IT-06")
                .departmentId(1L)
                .build();

        Mockito.when(departmentRepository.findByDepartmentNameIgnoreCase("IT"))
                .thenReturn(department);
    }

    @Test
    @DisplayName("Get Data based Valid Department Name")
    //@Disabled Annotation if you want to skip a Method for Testing
    public void whenValidDepartmentName_thenDepartmentShouldFound(){
        String departmentName = "IT";
        Department found = departmentService.fetchDepartmentByName(departmentName);
        assertEquals(departmentName, found.getDepartmentName());
    }
}
