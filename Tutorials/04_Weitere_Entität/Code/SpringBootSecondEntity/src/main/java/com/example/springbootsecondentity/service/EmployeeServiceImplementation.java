package com.example.springbootsecondentity.service;

import com.example.springbootsecondentity.entity.Employee;
import com.example.springbootsecondentity.error.EmployeeNotFoundException;
import com.example.springbootsecondentity.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class EmployeeServiceImplementation implements EmployeeService{

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> fetchEmployeeList() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee fetchEmployeeById(Long employeeId) throws EmployeeNotFoundException {
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        if(!employee.isPresent()){
            throw new EmployeeNotFoundException("Employee Not Available!");
        }else{
            return employee.get();
        }
    }

    @Override
    public void deleteEmployeeById(Long employeeId) {
        employeeRepository.deleteById(employeeId);
    }

    @Override
    public Employee updateEmployee(Long employeeId, Employee employee) throws EmployeeNotFoundException {
        Optional<Employee> employeeDatabase = employeeRepository.findById(employeeId);
        if (!employeeDatabase.isPresent()) {
            throw new EmployeeNotFoundException("Employee Not Available!");
        }
        /**
         * Weitere Möglichkeit / Schreibweise
         * Employee employee = employeeDatabase.get();
         */
        if (Objects.nonNull(employee.getEmployeeFirstName()) && !"".equalsIgnoreCase(employee.getEmployeeFirstName())) {
            employeeDatabase.get().setEmployeeFirstName(employee.getEmployeeFirstName());
        }
        if (Objects.nonNull(employee.getEmployeeLastName()) && !"".equalsIgnoreCase(employee.getEmployeeLastName())) {
            employeeDatabase.get().setEmployeeFirstName(employee.getEmployeeLastName());
        }
        return employeeRepository.save(employeeDatabase.get());
    }

    @Override
    public Employee fetchEmployeeByLastName(String employeeLastName) {
        return employeeRepository.findByEmployeeLastNameIgnoreCase(employeeLastName);
    }
}
