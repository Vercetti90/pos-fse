package com.example.springbootsecondentity.controller;

import com.example.springbootsecondentity.entity.Employee;
import com.example.springbootsecondentity.error.EmployeeNotFoundException;
import com.example.springbootsecondentity.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    public final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @PostMapping("/employee")
    public Employee saveEmployee(@Valid @RequestBody Employee employee){
        LOGGER.info("Inside saveEmployee of EmployeeController");
        return employeeService.saveEmployee(employee);
    }

    @GetMapping("/employee")
    public List<Employee> fetchEmployeeList(){
        LOGGER.info("Inside fetchEmployeeList of EmployeeController");
        return employeeService.fetchEmployeeList();
    }

    @GetMapping("/employee/{id}")
    public Employee fetchEmployeeById(@PathVariable("id")Long employeeId) throws EmployeeNotFoundException {
        return employeeService.fetchEmployeeById(employeeId);
    }

    @DeleteMapping("/employee/{id}")
    public String deleteEmployeeById(@PathVariable("id") Long employeeId){
        employeeService.deleteEmployeeById(employeeId);
        return "Employee deleted Successfully!";
    }

    @PutMapping("/employee/{id}")
    public Employee updateEmployee(@PathVariable("id") Long employeeId, @RequestBody Employee employee) throws EmployeeNotFoundException {
        return employeeService.updateEmployee(employeeId, employee);
    }
}

