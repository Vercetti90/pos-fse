package com.example.springbootsecondentity.repository;

import com.example.springbootsecondentity.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Employee findByEmployeeLastNameIgnoreCase(String employeeLastName);
}