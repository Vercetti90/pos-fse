package com.example.springbootsecondentity.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long employeeId;
    @NotBlank(message = "Please Add Employee Firstname!")
    private String employeeFirstName;
    @NotBlank(message = "Please Add Employee Lastname!")
    private String employeeLastName;

    //Referenzierung auf die Entität Department
    @ManyToOne
    private Department department;
}
