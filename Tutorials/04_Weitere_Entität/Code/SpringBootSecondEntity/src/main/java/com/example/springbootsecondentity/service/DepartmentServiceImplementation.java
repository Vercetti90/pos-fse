package com.example.springbootsecondentity.service;

import com.example.springbootsecondentity.entity.Department;
import com.example.springbootsecondentity.error.DepartmentNotFoundException;
import com.example.springbootsecondentity.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class DepartmentServiceImplementation implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    /**
     * This Method saves the data of a JSON-File
     * in our database (Enitity Department)
     * @param department
     * @return Enitity department
     */
    @Override
    public Department saveDepartment(Department department) {
        //A lot of Methods from the JPA Repository we have extended
        return departmentRepository.save(department);
    }

    /**
     * This Method fetches all the departments saved in our database.
     * The function findAll() gives a List of Departments back
     * @return All the departments in our Databse
     */
    @Override
    public List<Department> fetchDepartmentList()
    {
        return departmentRepository.findAll();
    }

    /**
     * This Method fetches the data of a department by the Department ID
     * We use the Method of the Repsository: findById()
     * @param departmentId
     * @return department
     */
    @Override
    public Department fetchDepartmentById(Long departmentId) throws DepartmentNotFoundException {
        Optional<Department> department = departmentRepository.findById(departmentId);
        if(!department.isPresent()){
            throw new DepartmentNotFoundException("Deparment Not Available!");
        }
        return department.get();
    }

    /**
     * This Method deletes a Department by ID
     * We use the Method of the Repsository: deleteById()
     * @param departmentId
     */
    @Override
    public void deleteDepartmentById(Long departmentId) {
        departmentRepository.deleteById(departmentId);
    }

    /**
     * This Method updates the Data of a Department
     * We put the Department we found by ID in a new Department Object: departmentDatabase
     * We make all the Null & Blank Checks
     * @param departmentId
     * @param department
     * @return
     */
    @Override
    public Department updateDepartment(Long departmentId, Department department) {
        Department departmentDatabase = departmentRepository.findById(departmentId).get();
        if(Objects.nonNull(department.getDepartmentName())&& !"".equalsIgnoreCase(department.getDepartmentName())){
            departmentDatabase.setDepartmentName(department.getDepartmentName());
        }
        if(Objects.nonNull(department.getDepartmentAddress())&& !"".equalsIgnoreCase(department.getDepartmentAddress())){
            departmentDatabase.setDepartmentAddress(department.getDepartmentAddress());
        }
        if(Objects.nonNull(department.getDepartmentCode())&& !"".equalsIgnoreCase(department.getDepartmentCode())){
            departmentDatabase.setDepartmentCode(department.getDepartmentCode());
        }
        return departmentRepository.save(departmentDatabase);
    }

    /**
     * NO PRE-DEFIND METHOD IN THE REPOSITORY!
     * Change in the DepartmentRepository Class and create the Method: fetchDepartmentByName!
     * This Method fetches a Department by Name
     * With the adding of "IgnoreCase" to the Methodname: findByDepartmentName
     * we will get the exact Data, even if we type "ce" instead of "CE"
     * This is how we can implement the different JPA-Methods
     * @param departmentName
     * @return department
     */
    @Override
    public Department fetchDepartmentByName(String departmentName) {
        //return departmentRepository.findByDepartmentName(departmentName);
        return departmentRepository.findByDepartmentNameIgnoreCase(departmentName);
    }
}
