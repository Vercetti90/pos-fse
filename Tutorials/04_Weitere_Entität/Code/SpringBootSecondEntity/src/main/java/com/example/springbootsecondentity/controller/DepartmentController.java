package com.example.springbootsecondentity.controller;

import com.example.springbootsecondentity.entity.Department;
import com.example.springbootsecondentity.error.DepartmentNotFoundException;
import com.example.springbootsecondentity.service.DepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    /**
     * Initialisierung des Loggers aus der SLF4J Library
     */
    public final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);

    /**
     * This method saves a Department in the database
     * Spring knows that we have all this classes (@AUTOWIRED
     * Annotation private DepartmentService departmentService;)
     * @param department
     */
    @PostMapping("/departments")
    public Department saveDepartment(@Valid @RequestBody Department department){
        //DepartmentService service = new DepartmentServiceImplementation();
        LOGGER.info("Inside saveDepartment of DepartmentController");
        return departmentService.saveDepartment(department);
    }

    /**
     * Method to get all the Departments from the Database.
     * We use the List Class and the Repository to return
     */
    @GetMapping("/departments")
    public List<Department> fetchDepartmentList(){
        LOGGER.info("Inside fetchDepartmentList of DepartmentController");
        return departmentService.fetchDepartmentList();
    }

    /**
     * The Method fetches the data of department by ID
     * Die eckigen Klammern um id stehen für einen dynamic Value
     * We have to map the variable departmentId
     * @return ID based department
     */
    @GetMapping("/departments/{id}")
    public Department fetchDepartmentById(@PathVariable("id")Long departmentId) throws DepartmentNotFoundException {
        return departmentService.fetchDepartmentById(departmentId);
    }

    /**
     * This Method deletes a Department by ID
     * We use the same Path Variable as for fetchingDepartmentById
     * Deleted successful, we return a Message
     * @param departmentId
     * @return Message
     */
    @DeleteMapping("/departments/{id}")
    public String deleteDepartmentById(@PathVariable("id") Long departmentId){
        departmentService.deleteDepartmentById(departmentId);
        return "Department deleted Successfully!";
    }

    /**
     * This Method updates a Department by ID
     * PutMapping is the Annotation for Updating
     * @param departmentId
     * @param department
     * @return department
     */
    @PutMapping("/departments/{id}")
    public Department updateDepartment(@PathVariable("id") Long departmentId, @RequestBody Department department){
        return departmentService.updateDepartment(departmentId, department);
    }

    /**
     * This Method fetches a Department by Name
     * @param departmentName
     * @return department
     */
    @GetMapping("/departments/name/{name}")
    public Department fetchDepartmentByName(@PathVariable("name") String departmentName){
        return departmentService.fetchDepartmentByName(departmentName);
    }
}

