package com.example.springbootsecondentity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSecondEntityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecondEntityApplication.class, args);
    }

}
