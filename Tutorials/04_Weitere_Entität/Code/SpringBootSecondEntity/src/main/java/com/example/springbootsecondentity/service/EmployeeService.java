package com.example.springbootsecondentity.service;

import com.example.springbootsecondentity.entity.Employee;
import com.example.springbootsecondentity.error.EmployeeNotFoundException;
import java.util.List;

public interface EmployeeService {

    Employee saveEmployee(Employee employee);

    List<Employee> fetchEmployeeList();

    Employee fetchEmployeeById(Long employeeId) throws EmployeeNotFoundException;

    void deleteEmployeeById(Long employeeId);

    Employee updateEmployee(Long employeeId, Employee employee) throws EmployeeNotFoundException;

    Employee fetchEmployeeByLastName(String employeeLastName);
}
