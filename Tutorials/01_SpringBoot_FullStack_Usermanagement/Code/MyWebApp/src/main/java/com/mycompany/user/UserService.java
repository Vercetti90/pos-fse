package com.mycompany.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

//SpringBoot to detect this class
@Service
public class UserService {
    //Referenz zum UserRepository
    @Autowired 
    private UserRepository repo;

    //1.Method: For listing all Users in the Database
    public List<User> listAll(){
        //findAll returns an Iterable Object, so we have to cast the return Type to List!
        return (List<User>)repo.findAll();
    }

    public void save(User user) {
        repo.save(user);
    }
    public User get(Integer id) throws UserNotFoundException {
        Optional<User> result = repo.findById(id);
        if(result.isPresent()){
            return result.get();
        }else{
            throw new UserNotFoundException("Could NOT find any Users with ID: "+id);
        }
    }
    //Method: For deleting User
    //First check if there is any user with the specified ID
    //Declar in the UserRepository Class
    public void delete(Integer id) throws UserNotFoundException {
        Long count = repo.countById(id);
        if(count == null || count == 0){
            throw new UserNotFoundException("Could NOT find any Users with ID: "+id);
        }else {
            repo.deleteById(id);
        }
    }
}
