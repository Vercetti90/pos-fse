package com.mycompany.user;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Integer> {
    //Name of the method is by convention
    public Long countById(Integer id);
}
