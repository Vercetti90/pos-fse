package com.mycompany;

import com.mycompany.user.User;
import com.mycompany.user.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.Optional;

//For the Test-Class
@DataJpaTest
//Test against the Real-Database instead of the Default-In-Memory-Database
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//We want to keep the data commited to the database
@Rollback(false)
public class UserRepositoryTest {

    //Reference to the UserRepository
    @Autowired private UserRepository repo;

    //1.Test: For testing we add a new User
    @Test
    public void testAddNew(){
        User user = new User();
        user.setEmail("philippmaar@web.de");
        user.setPassword("philipp123456");
        user.setFirstname("Philipp");
        user.setLastname("Maar");

        //save User in the Database (you find the save-Method in the CrudRepository Interface!
        User savedUser = repo.save(user);

        //For testing API
        Assertions.assertThat(savedUser).isNotNull();
        Assertions.assertThat(savedUser.getId()).isGreaterThan(0);
    }
    //2.Test: For listing users in the database
    @Test
    public void testListAll(){
        //You can find the findAll-Method in the CrudRepository Interface
        //Klick on the yellow Lamp --> Add local Variable --> Choose users!
        Iterable<User> users = repo.findAll();
        Assertions.assertThat(users).hasSizeGreaterThan(0);

        //Iterrieren über jedes User-Objekt
        //toString-Methode in der User-Klasse implementieren
        for (User user: users) {
            System.out.println(user);
        }
    }
    //3.Test: For updating the user
    @Test
    public void testUpdateUser(){
        //Variable für die userID
        Integer userId = 1;
        //findByID refered to the CrudRepository (Typ: Optional<>)
        Optional<User> optionalUser = repo.findById(userId);
        User user = optionalUser.get();
        user.setPassword("hello2000");
        //Save and Update the change into the database
        repo.save(user);

        //For test we can get the user back
        User updatedUser = repo.findById(userId).get();
        //Überprüfung mit Assertions
        //Bestimmte Annahmen über den Zustand des Programmes zu verifizieren und sicherzustellen, dass diese eingehalten werden!
        Assertions.assertThat(updatedUser.getPassword()).isEqualTo("hello2000");
    }
    //4.Test: Receiving User by ID
    @Test
    public void testGet(){
        Integer userId = 2;
        Optional<User> optionalUser = repo.findById(userId);
        Assertions.assertThat(optionalUser).isPresent();
        System.out.println(optionalUser.get());
    }
    //5.Test: Deleting User by ID
    @Test
    public void testDeleteUserById(){
        Integer userId = 2;
        repo.deleteById(userId);

        Optional<User> optionalUser = repo.findById(userId);
        Assertions.assertThat(optionalUser).isNotPresent();
    }
}
