package com.example.SpringBootWetterdaten.controller;

import com.example.SpringBootWetterdaten.entity.Messwert;
import com.example.SpringBootWetterdaten.exception.MesswertNotFoundException;
import com.example.SpringBootWetterdaten.exception.MesswertValidierungFehlgeschlagen;
import com.example.SpringBootWetterdaten.service.MesswertService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class MesswertController {

    @Autowired
    private MesswertService messwertService;

    public final Logger LOGGER = LoggerFactory.getLogger(MesswertController.class);


    /**
     * This method saves a Book in the database
     * @AUTOWIRED Annotation private LibraryService libraryService;
     * @param messwert
     */

//    Weitere / Alternative Möglichekeit!
//
//    @PostMapping("/messwerte")
//    public Messwert saveMesswert(@Valid @RequestBody Messwert messwert){
//        LOGGER.info("Inside saveCar of CarController");
//        return messwertService.saveMesswert(messwert);
//    }

    @PostMapping("/messwerte")
    public ResponseEntity<Messwert> saveMesswert(@Valid @RequestBody Messwert messwert, BindingResult bindingResult) throws MesswertNotFoundException, MesswertValidierungFehlgeschlagen {
        String errors = "";
        if(bindingResult.hasErrors())
        {
            for(ObjectError error : bindingResult.getAllErrors())
            {
                errors += "\nValidierungsfehler für Objekt " + error.getObjectName() +
                        " im Feld " + ((FieldError)error).getField() + " mit folgendem Problem: " +
                        error.getDefaultMessage();
            }
            throw new MesswertValidierungFehlgeschlagen(errors);
        } else {
            return ResponseEntity.ok(this.messwertService.saveMesswert(messwert));
        }
    }

    /**
     * Method to get all the Messwerte from the Database.
     * We use the List Class and the Repository to return
     */
    @GetMapping("/messwerte")
    public ResponseEntity<List<Messwert>> getAllMesswert(){
        LOGGER.info("Inside getAllMesswert of MesswertController");
        return ResponseEntity.status(HttpStatus.OK).body(messwertService.getAllMesswert());
    }

    /**
     * The Method fetches the data of Messwert by ID
     * Die eckigen Klammern um id stehen für einen dynamic Value
     * We have to map the variable id
     * @return ID based messwert
     */
    @GetMapping("/messwerte/{id}")
    public ResponseEntity<Messwert>getMesswertById (@PathVariable("id")Long id) throws MesswertNotFoundException {
        return ResponseEntity.status(HttpStatus.OK).body(messwertService.getAllMesswertById(id));
    }

    /**
     * This Method fetches a Messwert by Name
     * @param name
     * @return Messwert
     */
    @GetMapping("/messwerte/name/{name}")
    public ResponseEntity<List<Messwert>> getAllMesswertByName(@PathVariable("name") String name) {
        return ResponseEntity.status(HttpStatus.OK).body(messwertService.getAllMesswertByName(name));
    }

    /**
     * This Method deletes a Messwert by ID
     * We use the same Path Variable as for getMesswertById
     * Deleted successful, we return a Message
     * @param id
     * @return Message
     */
    @DeleteMapping("/messwerte/{id}")
    public String deleteMesswertById(@PathVariable("id") Long id) throws MesswertNotFoundException {
        this.messwertService.deleteMesswertById(id);
        return "Messwert deleted Successfully!";
    }
}
