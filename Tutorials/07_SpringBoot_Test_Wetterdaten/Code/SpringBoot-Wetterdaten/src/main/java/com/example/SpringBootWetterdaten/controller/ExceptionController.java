package com.example.SpringBootWetterdaten.controller;

import com.example.SpringBootWetterdaten.exception.ExceptionDTO;
import com.example.SpringBootWetterdaten.exception.MesswertNotFoundException;
import com.example.SpringBootWetterdaten.exception.MesswertValidierungFehlgeschlagen;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(MesswertNotFoundException.class)
    public ResponseEntity<ExceptionDTO> bookNotFound(MesswertNotFoundException bookNotFoundException)
    {
        return new ResponseEntity<>(new ExceptionDTO("1000",bookNotFoundException.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MesswertValidierungFehlgeschlagen.class)
    public ResponseEntity<ExceptionDTO> bookNotValidate(MesswertValidierungFehlgeschlagen bookValidierungFehlgeschlagen)
    {
        return new ResponseEntity<>(new ExceptionDTO("9000",bookValidierungFehlgeschlagen.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
