package com.example.SpringBootWetterdaten.entity;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Messwert {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank(message = "Please enter a weather station name!")
    @Size(min = 2)
    @Column(length = 45, nullable = false, name = "name")
    private String name;
    @NotBlank(message = "Please enter a value for weather station!")
    @Column(length = 15, nullable = false, name = "value")
    private double value;

    public Messwert(String name, double value) {
        this.name = name;
        this.value = value;
    }
}
