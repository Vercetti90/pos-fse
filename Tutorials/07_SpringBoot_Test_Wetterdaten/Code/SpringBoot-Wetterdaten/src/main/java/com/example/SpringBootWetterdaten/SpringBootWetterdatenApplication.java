package com.example.SpringBootWetterdaten;

import com.example.SpringBootWetterdaten.entity.Messwert;
import com.example.SpringBootWetterdaten.service.MesswertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWetterdatenApplication implements ApplicationRunner{

	@Autowired
	private MesswertService messwertService;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWetterdatenApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		messwertService.saveMesswert(new Messwert("Innsbruck",18.5));
		messwertService.saveMesswert(new Messwert("Innsbruck",18.5));
		messwertService.saveMesswert(new Messwert("Rosenheim",14.5));
		messwertService.saveMesswert(new Messwert("Rosenheim",14.5));
		messwertService.saveMesswert(new Messwert("Bozen",28.5));
		messwertService.saveMesswert(new Messwert("Bozen",26.5));
	}
}
