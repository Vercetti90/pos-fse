package com.example.SpringBootWetterdaten.service;

import com.example.SpringBootWetterdaten.entity.Messwert;
import com.example.SpringBootWetterdaten.exception.MesswertNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MesswertServiceImplementation implements MesswertService{

    @Autowired
    private DatabaseAccessMesswert databaseAccessMesswert;

    @Override
    public Messwert saveMesswert(Messwert messwert) {
        return databaseAccessMesswert.saveMesswert(messwert);
    }

    @Override
    public List<Messwert> getAllMesswert() {
        return databaseAccessMesswert.getAllMesswert();
    }

    @Override
    public List<Messwert> getAllMesswertByName(String name) {
        return databaseAccessMesswert.getAllMesswertByName(name);
    }

    @Override
    public Messwert getAllMesswertById(Long id) throws MesswertNotFoundException {
        return databaseAccessMesswert.getAllMesswertById(id);
    }

    @Override
    public void deleteMesswertById(Long id) throws MesswertNotFoundException {
        databaseAccessMesswert.deleteMesswertById(id);
    }
}
