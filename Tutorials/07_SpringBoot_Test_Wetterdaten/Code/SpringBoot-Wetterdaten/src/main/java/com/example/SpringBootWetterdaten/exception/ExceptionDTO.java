package com.example.SpringBootWetterdaten.exception;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ExceptionDTO {

    private String message;
    private String code;
}
