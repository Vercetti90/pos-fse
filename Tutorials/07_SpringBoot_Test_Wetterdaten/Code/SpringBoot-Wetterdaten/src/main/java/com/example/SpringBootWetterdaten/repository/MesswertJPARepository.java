package com.example.SpringBootWetterdaten.repository;

import com.example.SpringBootWetterdaten.entity.Messwert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository Interface
 *
 * Benötigt alle zusätlichen Methoden ausser CRUD!
 */
@Repository
public interface MesswertJPARepository extends JpaRepository<Messwert,Long> {

     List<Messwert> findAllByName(String name);
}


