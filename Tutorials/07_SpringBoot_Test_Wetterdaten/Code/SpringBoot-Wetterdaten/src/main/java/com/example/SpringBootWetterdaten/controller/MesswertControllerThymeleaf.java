package com.example.SpringBootWetterdaten.controller;

import com.example.SpringBootWetterdaten.entity.Messwert;
import com.example.SpringBootWetterdaten.exception.MesswertNotFoundException;
import com.example.SpringBootWetterdaten.service.MesswertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.util.List;

@Controller
@RequestMapping("")
public class MesswertControllerThymeleaf {

    @Autowired
    private MesswertService messwertService;


    @GetMapping("")
    public String showHomePage() {
        return "index.html";
    }

    @GetMapping("/web/showAllMesswerte")
    public String showMesswertList(Model model) {
        List<Messwert> listMesswerte = messwertService.getAllMesswert();
        model.addAttribute("messwerteList", listMesswerte);
        return "messwerte.html";
    }

    //Method: For handeling Add New Messwert
    //Referenz zu messwerte.html
    @GetMapping("/web/new")
    public String showNewForm(Model model) {
        model.addAttribute("messwert", new Messwert());
        model.addAttribute("pageTitle", "Add New Messwert");
        return "messwerte_form.html";
    }

    //Method: For save Messwert Data
    @PostMapping("/web/save")
    public String saveMesswert(Messwert messwert, RedirectAttributes redirectAttributes) {
        messwertService.saveMesswert(messwert);
        redirectAttributes.addFlashAttribute("message", "The Messwert has been successfully saved!");
        return "redirect:/web/showAllMesswerte";
    }

    //Method: For editing Messwert Data
    //Use special syntax for the ID
    @GetMapping("/web/edit/{id}")
    public String showEditForm(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttributes) {
        try {
            Messwert messwert = messwertService.getAllMesswertById(id);
            model.addAttribute("messwert", messwert);
            model.addAttribute("pageTitle", "Edit User with ID: " + id);
            return "messwerte_form.html";
        } catch (MesswertNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", e.getMessage());
            return "redirect:/web/showAllMesswerte";
        }
    }

    //Method: For deleting Messwert in the Listing Page
    @GetMapping("/web/delete/{id}")
    public String deleteMesswert(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
        try {
            messwertService.deleteMesswertById(id);
            redirectAttributes.addFlashAttribute("message", "The Messwert with ID " + id + " has been deleted!");
        } catch (MesswertNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", e.getMessage());
        }
        return "redirect:/web/showAllMesswerte";
    }
}

