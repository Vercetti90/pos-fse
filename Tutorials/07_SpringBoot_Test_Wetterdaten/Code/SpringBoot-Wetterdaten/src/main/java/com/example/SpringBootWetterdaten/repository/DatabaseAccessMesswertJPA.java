package com.example.SpringBootWetterdaten.repository;

import com.example.SpringBootWetterdaten.entity.Messwert;
import com.example.SpringBootWetterdaten.exception.MesswertNotFoundException;
import com.example.SpringBootWetterdaten.service.DatabaseAccessMesswert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;

@Component
public class DatabaseAccessMesswertJPA implements DatabaseAccessMesswert {

    @Autowired
    private MesswertJPARepository messwertJPARepository;

    @Override
    public Messwert saveMesswert(Messwert messwert) {
        return this.messwertJPARepository.save(messwert);
    }

    @Override
    public List<Messwert> getAllMesswert() {
        return this.messwertJPARepository.findAll();
    }

    @Override
    public List<Messwert> getAllMesswertByName(String name) {
        return this.messwertJPARepository.findAllByName(name);
    }

    @Override
    public Messwert getAllMesswertById(Long id) {
    return this.messwertJPARepository.getById(id);
    }

    @Override
    public void deleteMesswertById(Long id) throws MesswertNotFoundException {
        Optional<Messwert> messwert = messwertJPARepository.findById(id);
        if (!messwert.isPresent()) {
            throw new MesswertNotFoundException("Messwert Not Available!");
        } else {
            messwertJPARepository.deleteById(id);
        }
    }
}
