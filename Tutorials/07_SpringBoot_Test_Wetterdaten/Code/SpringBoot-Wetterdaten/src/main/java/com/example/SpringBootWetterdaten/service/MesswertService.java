package com.example.SpringBootWetterdaten.service;

import com.example.SpringBootWetterdaten.entity.Messwert;
import com.example.SpringBootWetterdaten.exception.MesswertNotFoundException;

import java.util.List;

/**
 * Business Logik
 *
 * Benötigt alle Methoden!
 */
public interface MesswertService {

    Messwert saveMesswert(Messwert messwert);
    List<Messwert> getAllMesswert();
    List<Messwert> getAllMesswertByName(String name);
    Messwert getAllMesswertById(Long id) throws MesswertNotFoundException;
    void deleteMesswertById(Long id) throws MesswertNotFoundException;
}
