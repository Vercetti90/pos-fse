package com.example.SpringBootWetterdaten.controller;

import com.example.SpringBootWetterdaten.entity.Messwert;
import com.example.SpringBootWetterdaten.service.MesswertService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.util.List;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(MesswertController.class)
public class ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MesswertService messwertService;
    private Messwert messwert;
    private List<Messwert> messwertList;

    @BeforeEach
    void setUp(){
        messwert = Messwert.builder()
                .id(1L)
                .name("Innsbruck")
                .value(19.5)
                .build();
    }
    @Test
    void getAllMesswertById() throws Exception {

        Mockito.when(messwertService.getAllMesswertById(messwert.getId())).thenReturn(messwert);

        //GET-REQUEST
        mockMvc.perform(get("/messwerte")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"name\":\"Innsbruck\",\n" +
                                "\t\"value\":\"19.0\"\n" +
                                "}"))
                .andExpect(status().isOk());
    }
    @Test
    void getAllMesswertByName() throws Exception {

        Mockito.when(messwertService.getAllMesswertByName(messwert.getName())).thenReturn(messwertList);

        //GET-REQUEST
        mockMvc.perform(get("/messwerte")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"name\":\"Innsbruck\",\n" +
                                "\t\"value\":\"19.0\"\n" +
                                "}"))
                .andExpect(status().isOk());
    }
    @Test
    void saveMesswert() throws Exception {
        Messwert inputMesswert = Messwert.builder()
                .name("Innsbruck")
                .value(19.5)
                .build();

        Mockito.when(messwertService.saveMesswert(inputMesswert))
                .thenReturn(messwert);
        //POST-Operation
        mockMvc.perform(post("/messwerte")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"name\":\"Innsbruck\",\n" +
                                "\t\"value\":\"19.0\"\n" +
                                "}"))
                .andExpect(status().isOk());
    }
}
