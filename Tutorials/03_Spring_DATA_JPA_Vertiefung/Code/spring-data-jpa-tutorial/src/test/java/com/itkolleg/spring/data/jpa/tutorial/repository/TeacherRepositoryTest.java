package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.Course;
import com.itkolleg.spring.data.jpa.tutorial.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
class TeacherRepositoryTest {

    @Autowired
    private TeacherRepository teacherRepository;

    @Test
    public void saveTeacher(){
        Course coursePOS = Course.builder()
                .title("POS")
                .credit(8)
                .build();

        Course courseFSE = Course.builder()
                .title("FSE")
                .credit(8)
                .build();

        Teacher teacher = Teacher.builder()
                .firstName("Claudio")
                .lastName("Landerer")
                //.courses(List.of(coursePOS,courseFSE))
                .build();

        teacherRepository.save(teacher);
    }
}