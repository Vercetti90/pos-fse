package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.Guardian;
import com.itkolleg.spring.data.jpa.tutorial.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
//@DataJpaTest = If you not want to store the Data in the Database
class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void saveStudent(){
        Student student = Student.builder()
                .emailId("tomislav@gmail.com")
                .firstName("Tomislav")
                .lastName("Kutz")
                //.guardianName("Nikhil")
                //.guardianEmail("nikhil@gmail.com")
                //.guardianMobile("9999999999")
                .build(); //To build this particular Object!

        studentRepository.save(student);
    }
    @Test
    public void saveStudentWithGuardian(){
        Guardian guardian = Guardian.builder()
                .name("Nikhil")
                .email("nikhil@gmail.com")
                .mobile("9999999999")
                .build();

        Student student = Student.builder()
                .firstName("Philipp")
                .emailId("mr-phil@gmail.com")
                .lastName("Maar")
                .guardian(guardian)
                .build();

        studentRepository.save(student);
    }

    @Test
    public void printAllStudent(){
        List<Student> studentList = studentRepository.findAll();
        System.out.println("Studentenliste: "+studentList);
    }

    @Test
    public void printStudentByFirstName(){
        List<Student> students = studentRepository.findByFirstName("philipp");
        System.out.println("Studenten: "+students);
    }

    @Test
    public void printStudentByFirstNameContaining(){
        List<Student> students = studentRepository.findByFirstNameContaining("ph");
        System.out.println("Studenten: "+students);
    }

    @Test
    public void printStudentBasedOnGuardianName(){
        List<Student> students = studentRepository.findByGuardianName("Nikhil");
        System.out.println("Studenten: "+students);
    }

    @Test
    public void printGetStudentByEmailAddress(){
        Student student = studentRepository
                .getStudentByEmailAddress("tomislav@gmail.com");
        System.out.println("Student: "+student);
    }

    @Test
    public void printGetStudentFirstNameByEmailAddress(){
        String firstName = studentRepository
                .getStudentFirstNameByEmailAddress("mr-phil@gmail.com");
        System.out.println("Firstname: "+firstName);
    }

    @Test
    public void printGetStudentByEmailAddressNative(){
        Student student = studentRepository
                .getStudentByEmailAddressNative("mr-phil@gmail.com");
        System.out.println("Student: "+student);
    }

    @Test
    public void printGetStudentByEmailAddressNativeNamedParam(){
        Student student = studentRepository
                .getStudentByEmailAddressNativeNamedParam("mr-phil@gmail.com");
        System.out.println("Student: "+student);
    }

    @Test
    public void updateStudentNameByEmailIdTest(){
        studentRepository.updateStudentNameByEmailId(
                "Thomas Update",
                "tomislav@gmail.com");
    }
}