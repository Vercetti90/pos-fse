package com.example.SpringBootLibrary;

import com.example.SpringBootLibrary.controller.BookController;
import com.example.SpringBootLibrary.entity.Book;
import com.example.SpringBootLibrary.service.LibraryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(BookController.class)
public class ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LibraryService libraryService;
    private Book book;

    @BeforeEach
    void setUp(){
        book = Book.builder()
                .id(1L)
                .title("Mercedes")
                .author("G63AMG")
                .pages(100)
                .bookAvailable(true)
                .build();
    }
    @Test
    void getBookById() throws Exception {

        Mockito.when(libraryService.getBookById(book.getId())).thenReturn(book);

        //GET-REQUEST
        mockMvc.perform(get("/books")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"title\":\"Mercedes\",\n" +
                                "\t\"author\":\"G63AMG\",\n" +
                                "\t\"pages\":\"100\"\n" +
                                "\t\"bookAvailable\":\"true\"\n" +
                                "}"))
                .andExpect(status().isOk());
    }

}
