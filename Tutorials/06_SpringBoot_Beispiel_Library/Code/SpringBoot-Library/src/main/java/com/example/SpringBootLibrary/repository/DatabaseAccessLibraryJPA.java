package com.example.SpringBootLibrary.repository;

import com.example.SpringBootLibrary.entity.Book;
import com.example.SpringBootLibrary.exception.AuthorNotFoundException;
import com.example.SpringBootLibrary.exception.BookNotFoundException;
import com.example.SpringBootLibrary.exception.TitleNotFoundException;
import lombok.Data;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
@Data
public class DatabaseAccessLibraryJPA implements DatabaseAccessLibrary {

    //Referenz JPA-Repository
    private LibraryJPARepository libraryJPARepository;

    /**
     * This Method returns all Books
     *
     * @return
     */
    @Override
    public List<Book> getAllBooks() {
        return libraryJPARepository.findAll();
    }

    /**
     * This Method returns all Books by ID
     *
     * @param bookId
     * @return
     * @throws BookNotFoundException
     */
    @Override
    public Book getBookById(Long bookId) throws BookNotFoundException {
        Optional<Book> book = libraryJPARepository.findById(bookId);
        if (!book.isPresent()) {
            throw new BookNotFoundException("Book Not Available!");
        } else {
            return book.get();
        }
    }

    /**
     * This Method saves a Book in the Database
     *
     * @param book
     * @return
     */
    @Override
    public Book saveBook(Book book) {
        return libraryJPARepository.save(book);
    }

    /**
     * This Method updates a Book by ID
     *
     * @param bookId
     * @param book
     * @return
     * @throws BookNotFoundException
     */
    @Override
    public Book updateBook(Long bookId, Book book) throws BookNotFoundException {
        Optional<Book> bookToUpdate = libraryJPARepository.findById(bookId);
        if (!bookToUpdate.isPresent()) {
            throw new BookNotFoundException("Book Not Available!");
        } else {
            if(Objects.nonNull(book.getAuthor())&& !"".equalsIgnoreCase(book.getAuthor())){
                bookToUpdate.get().setAuthor(book.getAuthor());
            }
            if(Objects.nonNull(book.getTitle())&& !"".equalsIgnoreCase(book.getTitle())){
                bookToUpdate.get().setTitle(book.getTitle());
            }
            if(Objects.nonNull(book.getPages())&& book.getPages() > 0){
                bookToUpdate.get().setPages(book.getPages());
            }
            if(Objects.nonNull(book.isBookAvailable())){
                bookToUpdate.get().setBookAvailable(book.isBookAvailable());
            }
        }
        return libraryJPARepository.save(bookToUpdate.get());
    }

    /**
     * This Method deletes all Books by ID
     *
     * @param bookId
     * @throws BookNotFoundException
     */
    @Override
    public void deleteBookById(Long bookId) throws BookNotFoundException {
        Optional<Book> book = libraryJPARepository.findById(bookId);
        if (!book.isPresent()) {
            throw new BookNotFoundException("Book Not Available!");
        } else {
            libraryJPARepository.delete(book.get());
        }
    }

    /**
     * This Method deletes all Books by Title
     *
     * @param title
     * @throws TitleNotFoundException
     */
    @Override
    public void deleteBookWithTitle(String title) throws TitleNotFoundException {
        List<Book> book = libraryJPARepository.findAll();
        for (Book books: book) {
            if(!books.getTitle().contains(title)) {
                throw new TitleNotFoundException("Title not Available!");
            } else {
                libraryJPARepository.deleteByTitle(title);
            }
        }
    }

    /**
     * This Method return all Book by Author
     *
     * @param author
     * @return
     * @throws AuthorNotFoundException
     */
    @Override
    public List<Book> findBookByAuthor(String author) throws AuthorNotFoundException {
        List<Book> book = libraryJPARepository.findAll();
        for (Book books : book) {
            if (!books.getAuthor().contains(author)) {
                throw new AuthorNotFoundException("Author not Available!");
            }
        }
        return libraryJPARepository.findAllByAuthor(author);
    }

    /**
     * This Method returns all available Books!
     *
     * @param title
     * @return
     * @throws BookNotFoundException
     */
    @Override
    public List<Book> availableBooks(String title) throws BookNotFoundException {
        List<Book> book = libraryJPARepository.findAll();
        for (Book books : book) {
            if (!books.getAuthor().contains(title)) {
                throw new BookNotFoundException("Book not Available!");
            }
        }
        return libraryJPARepository.findAllAvailableByTitle(title);
    }
}
