package com.example.SpringBootLibrary.exception;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ExceptionDTO {

    private String code;
    private String message;

}
