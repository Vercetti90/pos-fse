package com.example.SpringBootLibrary.controller;

import com.example.SpringBootLibrary.entity.Book;
import com.example.SpringBootLibrary.exception.AuthorNotFoundException;
import com.example.SpringBootLibrary.exception.BookNotFoundException;
import com.example.SpringBootLibrary.exception.BookValidierungFehlgeschlagen;
import com.example.SpringBootLibrary.exception.TitleNotFoundException;
import com.example.SpringBootLibrary.service.LibraryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BookController {

    @Autowired
    private LibraryService libraryService;

    public final Logger LOGGER = LoggerFactory.getLogger(BookController.class);

    /**
     * This method saves a Book in the database
     * @AUTOWIRED Annotation private LibraryService libraryService;
     * @param book
     */
    @PostMapping
    public ResponseEntity<Book> saveBook(@Valid @RequestBody Book book, BindingResult bindingResult) throws BookValidierungFehlgeschlagen {
        String errors = "";
        if(bindingResult.hasErrors())
        {
            for(ObjectError error : bindingResult.getAllErrors())
            {
                errors += "\nValidierungsfehler für Objekt " + error.getObjectName() +
                        " im Feld " + ((FieldError)error).getField() + " mit folgendem Problem: " +
                        error.getDefaultMessage();
            }
            throw new BookValidierungFehlgeschlagen(errors);
        } else {
            return ResponseEntity.ok(this.libraryService.saveBook(book));
        }
    }

    /**
     * Method to get all the Books from the Database.
     * We use the List Class and the Repository to return
     */
    @GetMapping("/books")
    public List<Book> getAllBooks(){
        LOGGER.info("Inside getAllBooks of BookController");
        return libraryService.getAllBooks();
    }

    /**
     * The Method fetches the data of Book by ID
     * Die eckigen Klammern um id stehen für einen dynamic Value
     * We have to map the variable carId
     * @return ID based book
     */
    @GetMapping("/books/{id}")
    public Book getBookById (@PathVariable("id")Long bookId) throws BookNotFoundException {
        return libraryService.getBookById(bookId);
    }

    /**
     * This Method fetches a Book by Author
     * @param author
     * @return Book
     */
    @GetMapping("/books/author/{author}")
    public List<Book> getAllBookByAuthor(@PathVariable("author") String author) throws AuthorNotFoundException {
        return libraryService.getAllBooksByAuthor(author);
    }

    /**
     * This Method deletes a Book by ID
     * We use the same Path Variable as for fetchCarById
     * Deleted successful, we return a Message
     * @param bookId
     * @return Message
     */
    @DeleteMapping("/books/{id}")
    public String deleteBookById(@PathVariable("id") Long bookId) throws BookNotFoundException {
        libraryService.deleteBookById(bookId);
        return "Book deleted Successfully!";
    }

    /**
     * This Method deletes a Book by Title
     * We use the same Path Variable as for getBookById
     * Deleted successful, we return a Message
     * @param title
     * @return Message
     */
    @DeleteMapping("/books/{title}")
    public String deleteBookByTitle(@PathVariable("title") String title) throws TitleNotFoundException {
        libraryService.deleteBookByTitle(title);
        return "Book deleted Successfully!";
    }

    /**
     * This Method updates a Book by ID
     * PutMapping is the Annotation for Updating
     * @param bookId
     * @param book
     * @return car
     */
    @PutMapping("/books/{id}")
    public Book updateBook(@PathVariable("id") Long bookId, @RequestBody Book book) throws  BookNotFoundException {
        return libraryService.updateBook(bookId, book);
    }

    /**
     * This Method checks if a Book is available by Title
     * @param title
     * @return Book
     */
    @GetMapping("/books/{bookAvailable}")
    public List<Book> isBookAvailableByTitle(@PathVariable("bookAvailable") String title) throws BookNotFoundException {
        return libraryService.isBookAvailableByTitle(title);
    }
}
