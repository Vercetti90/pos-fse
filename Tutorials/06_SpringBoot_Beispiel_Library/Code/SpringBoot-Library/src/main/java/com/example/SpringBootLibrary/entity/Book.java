package com.example.SpringBootLibrary.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Size(min = 2,max = 32)
    @NotBlank(message = "A author is required!")
    private String author;
    @NotBlank(message = "A title is required!")
    private String title;
    @Positive(message = "Please enter a number greater then Zero!")
    @Size(min = 2, max = 5300)
    @NotBlank(message = "Number of pages is required!")
    private int pages;
    @NotBlank(message = "Status must be set!")
    private boolean bookAvailable;
}
