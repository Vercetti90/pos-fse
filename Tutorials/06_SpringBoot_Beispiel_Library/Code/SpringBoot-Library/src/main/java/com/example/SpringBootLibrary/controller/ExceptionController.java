package com.example.SpringBootLibrary.controller;

import com.example.SpringBootLibrary.exception.BookNotFoundException;
import com.example.SpringBootLibrary.exception.BookValidierungFehlgeschlagen;
import com.example.SpringBootLibrary.exception.ExceptionDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(BookNotFoundException.class)
    public ResponseEntity<ExceptionDTO> bookNotFound(BookNotFoundException bookNotFoundException)
    {
        return new ResponseEntity<>(new ExceptionDTO("1000",bookNotFoundException.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BookValidierungFehlgeschlagen.class)
    public ResponseEntity<ExceptionDTO> bookNotValidate(BookValidierungFehlgeschlagen bookValidierungFehlgeschlagen)
    {
        return new ResponseEntity<>(new ExceptionDTO("9000",bookValidierungFehlgeschlagen.getMessage()), HttpStatus.BAD_REQUEST);
    }
}