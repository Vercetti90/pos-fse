package com.example.SpringBootLibrary.repository;

import com.example.SpringBootLibrary.entity.Book;
import com.example.SpringBootLibrary.exception.AuthorNotFoundException;
import com.example.SpringBootLibrary.exception.BookNotFoundException;
import com.example.SpringBootLibrary.exception.TitleNotFoundException;
import java.util.List;

/**
 * Repository Interface
 *
 * Benötigt alle Methoden!
 */
public interface DatabaseAccessLibrary {

    List<Book> getAllBooks();
    Book getBookById(Long bookId) throws BookNotFoundException;
    Book saveBook(Book book);
    Book updateBook(Long bookId, Book book) throws BookNotFoundException;
    void deleteBookById(Long bookId) throws BookNotFoundException;
    void deleteBookWithTitle(String title) throws TitleNotFoundException;
    List<Book> findBookByAuthor(String author) throws AuthorNotFoundException;
    List<Book> availableBooks(String title) throws BookNotFoundException;
}
