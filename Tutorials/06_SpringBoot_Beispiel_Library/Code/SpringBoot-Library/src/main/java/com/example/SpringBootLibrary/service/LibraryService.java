package com.example.SpringBootLibrary.service;

import com.example.SpringBootLibrary.entity.Book;
import com.example.SpringBootLibrary.exception.AuthorNotFoundException;
import com.example.SpringBootLibrary.exception.BookNotFoundException;
import com.example.SpringBootLibrary.exception.TitleNotFoundException;
import java.util.List;

/**
 * Business Logik
 *
 * Benötigt alle Methoden!
 */
public interface LibraryService {

    List<Book> getAllBooks();
    Book getBookById(Long bookId) throws BookNotFoundException;
    List<Book> getAllBooksByAuthor(String bookAuthor) throws AuthorNotFoundException;
    Book saveBook(Book book);
    void deleteBookById(Long bookId) throws BookNotFoundException;
    void deleteBookByTitle(String title) throws TitleNotFoundException;
    Book updateBook(Long bookId, Book book) throws BookNotFoundException;
    List<Book> isBookAvailableByTitle(String title) throws BookNotFoundException;
}
