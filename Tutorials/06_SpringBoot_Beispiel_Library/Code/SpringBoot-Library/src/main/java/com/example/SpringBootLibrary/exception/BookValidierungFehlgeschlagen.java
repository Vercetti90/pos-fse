package com.example.SpringBootLibrary.exception;

public class BookValidierungFehlgeschlagen extends Exception {

    public BookValidierungFehlgeschlagen(String message){
        super(message);
    }
}
