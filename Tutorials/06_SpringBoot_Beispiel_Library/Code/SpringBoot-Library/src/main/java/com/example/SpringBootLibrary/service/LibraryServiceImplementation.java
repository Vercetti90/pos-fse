package com.example.SpringBootLibrary.service;

import com.example.SpringBootLibrary.entity.Book;
import com.example.SpringBootLibrary.exception.AuthorNotFoundException;
import com.example.SpringBootLibrary.exception.BookNotFoundException;
import com.example.SpringBootLibrary.exception.TitleNotFoundException;
import com.example.SpringBootLibrary.repository.DatabaseAccessLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class LibraryServiceImplementation implements LibraryService {

    @Autowired
    private DatabaseAccessLibrary databaseAccessLibrary;

    @Override
    public List<Book> getAllBooks() {
        return databaseAccessLibrary.getAllBooks();
    }

    @Override
    public Book getBookById(Long bookId) throws BookNotFoundException {
        return databaseAccessLibrary.getBookById(bookId);
    }

    @Override
    public List<Book> getAllBooksByAuthor(String bookAuthor) throws AuthorNotFoundException {
        return databaseAccessLibrary.findBookByAuthor(bookAuthor);
    }

    @Override
    public Book saveBook(Book book) {
        return databaseAccessLibrary.saveBook(book);
    }

    @Override
    public void deleteBookById(Long bookId) throws BookNotFoundException {
        databaseAccessLibrary.deleteBookById(bookId);
    }

    @Override
    public void deleteBookByTitle(String title) throws TitleNotFoundException {
        databaseAccessLibrary.deleteBookWithTitle(title);
    }

    @Override
    public Book updateBook(Long bookId, Book book) throws BookNotFoundException {
        return databaseAccessLibrary.updateBook(bookId,book);
    }

    @Override
    public List<Book> isBookAvailableByTitle(String title) throws BookNotFoundException {
        return databaseAccessLibrary.availableBooks(title);
    }
}
