package com.example.SpringBootLibrary.exception;

public class BookNotFoundException extends Exception{

    public BookNotFoundException(String message){
        super(message);
    }
}
