package com.example.SpringBootLibrary.repository;

import com.example.SpringBootLibrary.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Repository Interface
 *
 * Benötigt alle zusätlichen Methoden ausser CRUD!
 */
@Repository
public interface LibraryJPARepository extends JpaRepository<Book,Long> {

    List<Book> findAllByAuthor(String bookAuthor);
    void deleteByTitle(String title);
    List<Book> findAllAvailableByTitle(String title);
}
