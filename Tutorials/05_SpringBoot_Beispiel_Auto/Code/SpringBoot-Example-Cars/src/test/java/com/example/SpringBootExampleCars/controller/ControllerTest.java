package com.example.SpringBootExampleCars.controller;

import com.example.SpringBootExampleCars.entity.Car;
import com.example.SpringBootExampleCars.service.CarService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CarController.class)
public class ControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CarService carService;
    private Car car;

    @BeforeEach
    void setUp(){
        car = Car.builder()
                .carId(1L)
                .carBrand("Mercedes")
                .carModel("G63 AMG")
                .carYearOfConstruction("2021")
                .build();
    }
    @Test
    void saveCar() throws Exception {
        Car inputCar = Car.builder()
                .carBrand("Mercedes")
                .carModel("G63 AMG")
                .carYearOfConstruction("2021")
                .build();

        Mockito.when(carService.carSave(inputCar)).thenReturn(car);

        //POST-REQUEST
        mockMvc.perform(post("/cars")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"carBrand\":\"Mercedes\",\n" +
                                "\t\"carModel\":\"G63 AMG\",\n" +
                                "\t\"carYearOfConstruction\":\"2021\"\n" +
                                "}"))
                .andExpect(status().isOk());
    }
}
