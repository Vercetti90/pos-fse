package com.example.SpringBootExampleCars.exception;

public class CarNotFoundException extends Exception{

    public CarNotFoundException(String message){
        super(message);
    }
}
