package com.example.SpringBootExampleCars.service;

import com.example.SpringBootExampleCars.entity.Car;
import com.example.SpringBootExampleCars.exception.CarNotFoundException;

import java.util.List;

public interface CarService {

    Car carSave(Car car);

    List<Car> fetchCarList();

    Car fetchCarById(Long carId) throws CarNotFoundException;

    void deleteCarById(Long carId) throws CarNotFoundException;

    Car updateCar(Long carId, Car car) throws CarNotFoundException;

    Car fetchCarByBrand(String brand);
}
