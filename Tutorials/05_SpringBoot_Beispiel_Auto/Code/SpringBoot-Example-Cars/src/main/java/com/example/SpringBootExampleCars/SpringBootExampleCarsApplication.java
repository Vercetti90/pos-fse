package com.example.SpringBootExampleCars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootExampleCarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootExampleCarsApplication.class, args);
	}

}
