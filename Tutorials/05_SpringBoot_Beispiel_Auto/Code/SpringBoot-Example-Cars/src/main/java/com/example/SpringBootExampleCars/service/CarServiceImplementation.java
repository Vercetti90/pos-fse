package com.example.SpringBootExampleCars.service;

import com.example.SpringBootExampleCars.entity.Car;
import com.example.SpringBootExampleCars.exception.CarNotFoundException;
import com.example.SpringBootExampleCars.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class CarServiceImplementation implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Override
    public Car carSave(Car car) {
        return carRepository.save(car);
    }

    @Override
    public List<Car> fetchCarList() {
        return carRepository.findAll();
    }

    @Override
    public Car fetchCarById(Long carId) throws CarNotFoundException {
        Optional<Car> car = carRepository.findById(carId);
        if (!car.isPresent()) {
            throw new CarNotFoundException("Car Not Available!");
        } else {
            return car.get();
        }
    }

    @Override
    public void deleteCarById(Long carId) throws CarNotFoundException {
        Optional<Car> car = carRepository.findById(carId);
        if (!car.isPresent()) {
            throw new CarNotFoundException("Car Not Available!");
        } else {
            carRepository.delete(car.get());
        }
    }

    @Override
    public Car updateCar(Long carId, Car car) throws CarNotFoundException {
        Optional<Car> carToUpdate = carRepository.findById(carId);
        if (!carToUpdate.isPresent()) {
            throw new CarNotFoundException("Car Not Available!");
        } else {
            if(Objects.nonNull(car.getCarBrand())&& !"".equalsIgnoreCase(car.getCarBrand())){
                carToUpdate.get().setCarBrand(car.getCarBrand());
            }
            if(Objects.nonNull(car.getCarModel())&& !"".equalsIgnoreCase(car.getCarModel())){
                carToUpdate.get().setCarModel(car.getCarModel());
            }
            if(Objects.nonNull(car.getCarYearOfConstruction())&& !"".equalsIgnoreCase(car.getCarYearOfConstruction())){
                carToUpdate.get().setCarYearOfConstruction(car.getCarYearOfConstruction());
            }
        }
        return carRepository.save(carToUpdate.get());
    }

    @Override
    public Car fetchCarByBrand(String brand) {
        return carRepository.findByCarBrand(brand);
    }
}
