package com.example.SpringBootExampleCars.controller;

import com.example.SpringBootExampleCars.entity.Car;
import com.example.SpringBootExampleCars.exception.CarNotFoundException;
import com.example.SpringBootExampleCars.service.CarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CarController {

    @Autowired
    private CarService carService;

    public final Logger LOGGER = LoggerFactory.getLogger(CarController.class);

    /**
     * This method saves a Car in the database
     * @AUTOWIRED Annotation private CarService departmentService;
     * @param car
     */
    @PostMapping("/cars")
    public Car saveCar(@Valid @RequestBody Car car){
        LOGGER.info("Inside saveCar of CarController");
        return carService.carSave(car);
    }

    /**
     * Method to get all the Cars from the Database.
     * We use the List Class and the Repository to return
     */
    @GetMapping("/cars")
    public List<Car> fetchCarList(){
        LOGGER.info("Inside fetchDepartmentList of DepartmentController");
        return carService.fetchCarList();
    }

    /**
     * The Method fetches the data of Car by ID
     * Die eckigen Klammern um id stehen für einen dynamic Value
     * We have to map the variable carId
     * @return ID based car
     */
    @GetMapping("/cars/{id}")
    public Car fetchCarById(@PathVariable("id")Long carId) throws CarNotFoundException {
        return carService.fetchCarById(carId);
    }

    /**
     * This Method fetches a Car by Brand
     * @param carBrand
     * @return car
     */
    @GetMapping("/cars/brand/{brand}")
    public Car fetchCarByBrand(@PathVariable("carBrand") String carBrand){
        return carService.fetchCarByBrand(carBrand);
    }

    /**
     * This Method deletes a Car by ID
     * We use the same Path Variable as for fetchCarById
     * Deleted successful, we return a Message
     * @param carId
     * @return Message
     */
    @DeleteMapping("/cars/{id}")
    public String deleteCarById(@PathVariable("id") Long carId) throws CarNotFoundException {
        carService.deleteCarById(carId);
        return "Car deleted Successfully!";
    }

    /**
     * This Method updates a Car by ID
     * PutMapping is the Annotation for Updating
     * @param carId
     * @param car
     * @return car
     */
    @PutMapping("/cars/{id}")
    public Car updateCar(@PathVariable("id") Long carId, @RequestBody Car car) throws CarNotFoundException {
        return carService.updateCar(carId, car);
    }
}
