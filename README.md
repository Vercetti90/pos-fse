# Fullstack Development mit Spring Boot (BACKEND-Technology)

# Inhaltsverzeichnis
- [Fullstack Development mit Spring Boot (BACKEND-Technology)](#fullstack-development-mit-spring-boot-backend-technology)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [DOKUMENTATION (GitLab)](#dokumentation-gitlab)
  - [Anmerkungen](#anmerkungen)
  - [Tutorials](#tutorials)
    - [SPRING BOOT FULL STACK USERMANAGEMENT](#spring-boot-full-stack-usermanagement)
      - [Technologien](#technologien)
      - [Software Programs](#software-programs)
      - [Erwartetes Ergebnis](#erwartetes-ergebnis)
      - [Klassendiagramm](#klassendiagramm)
      - [Allgemeine Vorgehensweise](#allgemeine-vorgehensweise)
      - [Create & Setup Spring Boot Project](#create--setup-spring-boot-project)
      - [Create New Project](#create-new-project)
      - [Configure DataSource Properties](#configure-datasource-properties)
      - [Code Homepage](#code-homepage)
      - [Anleitung Development](#anleitung-development)
    - [SPRING BOOT FULL STACK DEPARTMENTMANAGEMENT](#spring-boot-full-stack-departmentmanagement)
      - [Introduction](#introduction)
      - [Dependency Injection (Einbringen von Abhängigkeiten)](#dependency-injection-einbringen-von-abhängigkeiten)
      - [Spring Initializr](#spring-initializr)
      - [Simple API (Application Programming Interface)](#simple-api-application-programming-interface)
      - [Add Spring Boot Devtools](#add-spring-boot-devtools)
      - [Architecture and Examples](#architecture-and-examples)
      - [H2 and JPA Dependency](#h2-and-jpa-dependency)
      - [Creating Components](#creating-components)
      - [Department Save API](#department-save-api)
      - [Testing APIs using REST CLIENT](#testing-apis-using-rest-client)
      - [GetMapping - Fetching data from Database](#getmapping---fetching-data-from-database)
      - [Fetching Data by ID](#fetching-data-by-id)
      - [Deleting Data](#deleting-data)
      - [Updating Data](#updating-data)
      - [Fetch Data by Name](#fetch-data-by-name)
      - [HIBERNATE Validation](#hibernate-validation)
      - [Adding Loggers](#adding-loggers)
      - [Project Lombok (Removing Boiler plate code)](#project-lombok-removing-boiler-plate-code)
      - [Exception Handling](#exception-handling)
      - [Changing Database: H2 ⟶ MySQL](#changing-database-h2--mysql)
      - [UNIT Testing](#unit-testing)
        - [UNIT Testing Service Layer](#unit-testing-service-layer)
        - [UNIT Testing Repository Layer](#unit-testing-repository-layer)
        - [UNIT Testing Controller Layer](#unit-testing-controller-layer)
      - [Adding Config in Properties-File](#adding-config-in-properties-file)
      - [Adding application.yml](#adding-applicationyml)
      - [Springboot Profiles](#springboot-profiles)
      - [Running SpringBoot with multiple Profiles](#running-springboot-with-multiple-profiles)
      - [SpringBoot Actuator](#springboot-actuator)
      - [Custom Actuator Endpoint](#custom-actuator-endpoint)
      - [Exclude Actuator Endpoint](#exclude-actuator-endpoint)
    - [SPRING DATA JPA VERTIEFUNG](#spring-data-jpa-vertiefung)
      - [What we Build](#what-we-build)
      - [Connecting SpringBoot App with Database](#connecting-springboot-app-with-database)
      - [Mapping Entities with Database](#mapping-entities-with-database)
      - [Different JPA Annotations](#different-jpa-annotations)
      - [Understanding Repositories and Methods](#understanding-repositories-and-methods)
      - [@Embeddable and @Embedded](#embeddable-and-embedded)
      - [Creating JPA Repositories and Methods](#creating-jpa-repositories-and-methods)
      - [JPA @Query Annotation](#jpa-query-annotation)
      - [Native Queries Example](#native-queries-example)
      - [Query Named Params](#query-named-params)
      - [@Transactional and @Modifying Annotation](#transactional-and-modifying-annotation)
      - [JPA One to One Relationship](#jpa-one-to-one-relationship)
      - [Cascade Types](#cascade-types)
      - [Fetch Types](#fetch-types)
      - [Uni & Bi directional Relationship](#uni--bi-directional-relationship)
      - [JPA One to Many Relationship](#jpa-one-to-many-relationship)
      - [JPA Many to One Relationship](#jpa-many-to-one-relationship)
      - [Paging and Sorting](#paging-and-sorting)
      - [JPA Many to Many Relationship](#jpa-many-to-many-relationship)
      - [DTO (Datentransferobjekt oder Transferobjekt)](#dto-datentransferobjekt-oder-transferobjekt)
  - [WEITERE ENTITÄT HINZUFÜGEN](#weitere-entität-hinzufügen)
    - [Erweiterung](#erweiterung)
    - [Vorgehensweise](#vorgehensweise)
      - [Implementierung der Entität Employee](#implementierung-der-entität-employee)
      - [Bearbeitung der Entität Department](#bearbeitung-der-entität-department)
      - [EmployeeController Klasse](#employeecontroller-klasse)
      - [EmployeeService Interface](#employeeservice-interface)
      - [EmployeeServiceImplementation Klasse](#employeeserviceimplementation-klasse)
      - [Implementierung der Test-Klassen](#implementierung-der-test-klassen)
        - [EmployeeControllerTest](#employeecontrollertest)
        - [EmployeeRepositoryTest](#employeerepositorytest)
        - [EmployeeServiceTest](#employeeservicetest)
  - [SpringBoot Exercises](#springboot-exercises)
    - [Beispiel Car](#beispiel-car)
    - [Beispiel Library](#beispiel-library)
    - [Beispiel Wetterstation](#beispiel-wetterstation)


## DOKUMENTATION (GitLab)

https://gitlab.com/Vercetti90/pos-fse

Die Ausarbeitung und Dokumentation wird in folgender Datei festgehalten: 

    README.md 

## Anmerkungen

+ **STANDARD-ARCHITEKTUR**

![Standardarchitektur](images/2021-10-20_Standardarchitektur.jpg)

+ **CQRS** (Command Query Responsibility Segregation) = „Kommando-Abfrage-Zuständigkeitstrennung“. Entwurfsmuster für Datenbank-Abfragen (Greg Young)

## Tutorials

Die Ausarbeitung und der Code der einzelnen Tutorials befindet sich im Ordner *Tutorials*

### SPRING BOOT FULL STACK USERMANAGEMENT

https://www.youtube.com/watch?v=u8a25mQcMOI

What you will learn:

+ Develop a Java Web Application based on Spring Framework
+ Code User Module with **CRUD** operations (**Create**, **Retrieve = Abrufen**, **Update**, **Delete**)

#### Technologien 

+ **Spring Boot Web** (= Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run". Eines der wichtigsten Features von Spring Boot ist die **Autokonfiguration**.)
+ **Sprin Data JPA** (= Spring Data JPA provides repository support for the Java Persistence API (JPA). It eases development of applications that need to access JPA data sources.)
+ **Hibernate** (= Bei Hibernate handelt es sich um ein Framework zur Abbildung von Objekten auf relationalen Datenbanken für die Programmiersprache Java - es wird auch als Object Relational Mapping Tool bezeichnet. Hibernate ist in der Programmiersprache Java implementiert. Hibernate nutzt das Konzept der Reflexion, um so zur Laufzeit einer Software sogenannte Meta-Informationen über die Struktur von Objekten und die zugrunde liegenden Klassen zu ermitteln, die dann auf die Tabellen einer Datenbank abgebildet werden.)
+ **MySQL Database** (= is a database management system)
+ **Thymeleaf** (= serverseitige Template-Engine und ähnelt damit den bekannten Java Server Pages. Anders als diese können Thymeleaf-Templates aber ohne Probleme im Browser angezeigt und mit gängigen Designtools bearbeitet werden.)
+ **Bootstrap** (= Quickly design and customize responsive mobile-first sites with Bootstrap, the world’s most popular front-end open source toolkit, featuring Sass variables and mixins, responsive grid system, extensive prebuilt components, and powerful JavaScript plugins)
+ **HTML5** (= die fünfte Fassung der Hypertext Markup Language (engl. für Hypertext-Auszeichnungssprache), einer Computersprache zur Auszeichnung und Vernetzung von Texten und anderen Inhalten elektronischer Dokumente, vorwiegend im World Wide Web.)
+ **JUnit 5** (= ein Framework zum Testen von Java-Programmen, das besonders für automatisierte Unit-Tests einzelner Units (Klassen oder Methoden) geeignet ist.)
+ **AssertJ** (= an opensource community-driven library used for writing fluent and rich assertions in Java tests.)

#### Software Programs

+ Java Development Kit (OpenJDK)
+ IntelliJ IDEA Ultimate
+ MySQL Community Server
+ MySQL Workbench

#### Erwartetes Ergebnis

![Ergebnis1](images/2021-10-10_Erwartetes_Ergebnis_1.jpg)

![Ergebnis2](images/2021-10-10_Erwartetes_Ergebnis_2.jpg)

![Ergebnis3](images/2021-10-10_Erwartetes_Ergebnis_3.jpg)

#### Klassendiagramm

![Klassendiagramm](images/2021-10-10_Klassendiagramm.jpg)

#### Allgemeine Vorgehensweise

1. Create & Setup Spring Boot Project
2. Code Homepage
3. Code User Entity Class & UserRepository Interface
4. Code UserRepositoryTests (unit tests for Data-Access-Layer)
5. Code Users Listing Page
6. Code **ADD** User Function
7. Code **UPDATE** User Function
8. Code **DELETE** User Function

#### Create & Setup Spring Boot Project

1. Declare dependencies for:
   + Spring Web
   + Spring Data JPA
   + Thymeleaf 
   + MySQL JDBC Driver
   + Springt Boot DevTools (for automatic reload changes)
   + Webjars for Bootstrap

2. Create new Database Schema

3. Configure Datasource in **application.properties**

#### Create New Project

1. Create new Project

![New-Project](images/2021-10-10_New_Project.jpg)

2. Add Dependencies

![Add-Dependencies](images/2021-10-10_Add_Dependencies.jpg)

3. Project

![Project_01](images/2021-10-10_01_Project.jpg)

4. Database

Anlegen über den Reiter ganz rechts:

 **Database** und dann auf **+** --> **New Datasource** --> **MySQL**

![Database_01](images/2021-10-10_Database_Configuration.jpg)

Neues **Schema (usersdb)** anlegen

![Database_02](images/2021-10-10_Database_Configuration2.jpg)

Name: *usersdb* und unter dem Punkt **Collation** folgendes eintragen:

    utf8mb4_general_ci

![Database_03](images/2021-10-10_Database_Configuration3.jpg)

#### Configure DataSource Properties

In der Datei **application.properties** folgende Parameter einfügen:

```java
spring.datasource.url=jdbc:mysql://localhost:3306/usersdb
server.port=8888
spring.datasource.username=root
spring.datasource.password=123
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.show_sql=true
```
Hinweis:

    SpringBoot verwendet Out-of-the-Box den Port 8080. Da dieser in der Docker Umgebung bereits für Adminer belegt ist, muss auf einen anderen Port ausgewichen werden, mit Hilfe der Zeile: server.port=8888

![Application-Properties](images/2021-10-10_Application_Properties.jpg)

#### Code Homepage

#### Anleitung Development

1. Code MainController class (Create New Class: *MainController*)
2. Code index.html

![Homepage-Index](images/2021-10-10_Homepage_Index.jpg)

3. Run the Application

![Homepage-Test](images/2021-10-10_Homepage_Test.jpg)

4. Configure IDE to use DevTools (automatic reload changes)

       Diese Konfiguration dient dem automatischen Laden von Änderungen. Ansonsten muss das Programm jedesmal gestoppt und erneut gestartet werden!

Tipp:

```
Strg + Umschalt + A wechselt in die Hilfe und Suchfunktion von IntelliJ
```
![DevTools1](images/2021-10-10_DevTools.jpg)

![DevTools2](images/2021-10-10_DevTools2.jpg)

![DevTools3](images/2021-10-10_DevTools3.jpg)

Hinweis:

Aufgrund der neueren Version lässt sich in der Registry der Eintrag: *compiler.automake.allow.while.running* nicht mehr auffinden! Hilfe unter **Settings** ⟶ **Advanced Settings**

![DevTools4](images/2021-10-10_DevTools4.jpg)

5. Use Bootstrap for Project

To use Bootstrap, we have to implement some dependencies in the *pom.xml*-File at the end of the Dependency-Section:

```xml
        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>bootstrap</artifactId>
            <version>4.3.1</version>
        </dependency>
        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>webjars-locator-core</artifactId>
        </dependency>
```
After this, we have to make some changes in the *index.html*-File (**Template**):

```html
Absolute URLs
<html lang="en" xmlns:th="http://www.thymeleaf.org">

Context-Relative URLs
<link rel="stylesheet" type="text/css" th:href="@{/webjars/bootstrap/css/bootstrap.min.css}"/>

```

    The Thymeleaf standard dialects – called Standard and SpringStandard – offer a way to easily create URLs in your web applications so that they include any required URL preparation artifacts. This is done by means of the so-called link expressions, a type of Thymeleaf Standard Expression: @{...}

Some things to note here:

+ **th:href** is a modifier attribute: once processed, it will compute the link URL to be used and set that value to the href attribute of the < a > tag.
+ We are allowed to use expressions for URL parameters (as you can see in orderId=${o. id}). The required URL-parameter-encoding operations will also be automatically performed.
+ If several parameters are needed, these will be separated by commas: @{/order/process(execId=${execId},execType='FAST')}
+ Variable templates are also allowed in URL paths: @{/order/{orderId}/details(orderId=${orderId})}
+ Relative URLs starting with / (eg: /order/details) will be automatically prefixed by the application context name.
+ If cookies are not enabled or this is not yet known, a ";jsessionid=..." suffix might be added to relative URLs so that the session is preserved. This is called URL Rewriting and Thymeleaf allows you to plug in your own rewriting filters by using the response.encodeURL(...) mechanism from the Servlet API for every URL.
+ The th:href attribute allows us to (optionally) have a working static href attribute in our template, so that our template links remained navigable by a browser when opened directly for prototyping purposes.

Tipp:

    Sometimes you need to RELOAD MAVEN after making changes on Depedencies etc.

![Bootstrap-Dependencies](images/2021-10-12_Bootstrap_Dependencies.jpg)

![Bootstrap-Min-CSS](images/2021-10-12_Bootstrap_Min_CSS.jpg)

Anpassung des Designs der Homepage mit Hilfe von Bootstrap, sowie die **Thymeleaf Verlinkung**!

```html
<div class="container-fluid text-center">
    <h1>Welcome to My Application</h1>
    <!--Thymeleaf Verlinkung th:href="@{...}-->
    <a class="h2" th:href="@{/users}">Manage Users</a>
```
Tipp:

    Damit die Links aktualisiert werden, müssen wir das Programm zunächst manuell beenden und anschließend Neustarten!

![Bootstrap-Index](images/2021-10-12_Bootstrap_Index.jpg)

6. Code Data Access Layer for User module (Repository Layer)

     1. Code User entity class that maps to users table in Database
     2. Code UserRepository interface to use APIs of Spring Data JPA
     3. Run the application to let Hibernate creates the table

+ Wir erstellen ein neues Package *user* und darin implementieren wir eine neue Java-Class **User**

![User-Class](images/2021-10-12_User_Class.jpg)

Anschließend müssen einige JPA-Notations hinzugefügt werden:

```java
package com.mycompany.user;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //Email wird dadurch required und darf nur einmal vorkommen!
    @Column(nullable = false, unique = true,length = 45)
    private String email;

    //Länge des Passworts auf 15 gesetzt
    @Column(length = 15, nullable = false)
    private String password;

    //Mit "name" kann ich der Spalte einen Namen geben, ansonsten wird einfach der Variablenname genommen by default!
    @Column(length = 45, nullable = false,name = "first_name")
    private String firstname;

    @Column(length = 45, nullable = false,name = "last_name")
    private String lastname;
    
}
```
Als nächstes implementieren wir noch **GETTER- und SETTER-Methoden** für die einzelnen Datenfelder

![Getter-Setter](images/2021-10-12_Getter_Setter.jpg)

![Getter-Setter2](images/2021-10-12_Getter_Setter2.jpg)

+ Anlegen eines neuen Java Interface **UserRepository**. Dieses erbt vom CrudRepository (Strg + Mausklick auf CrudRepository ⟶ INFO)

```java
package com.mycompany.user;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Integer> {

}
```
Überprüfung der Datenbank über **localhost:8082**

![MySQL1](images/2021-10-13_MySQL.jpg)

![MySQL2](images/2021-10-13_MySQL2.jpg)

![MySQL3](images/2021-10-13_MySQL3.jpg)

Beim Beenden und Starten der Programms, sorgt **HIBERNATE** dafür das die Tabellen erstellt werden!

![MySQL4](images/2021-10-13_MySQL4.jpg)

    With IntelliJ we dont need MySQL-Workbench or other programms. Inside of IntelliJ we got the Database, a powerfull function of IntelliJ!

+ Code Unit Tests for Data Access Layer (UserRepositoryTest)

    1. Use Spring Data JPA Test
    2. Test CRUD operations on users
    3. Run Tests against real database (default is in-memory database)
   
      Follow TDD (Test Driven Development)

**Directory test** --> **Delete MyWebAppApplicationTests Class** -->**Create new Java Class UserRepositoryTest**

1. Test adding new User in the Database:

```java
package com.mycompany;

import com.mycompany.user.User;
import com.mycompany.user.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

//For the Test-Class
@DataJpaTest
//Test against the Real-Database instead of the Default-In-Memory-Database
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//We want to keep the data commited to the database
@Rollback(false)
public class UserRepositoryTest {

    //Reference to the UserRepository
    @Autowired private UserRepository repo;

    //For testing we add a new User
    @Test
    public void testAddNew(){
        User user = new User();
        user.setEmail("tommykutz@web.de");
        user.setPassword("tommy123456");
        user.setFirstname("Thomas");
        user.setLastname("Kutz");

        //save User in the Database (you find the save-Method in the CrudRepository!
        User savedUser = repo.save(user);

        //For testing API
        Assertions.assertThat(savedUser).isNotNull();
        Assertions.assertThat(savedUser.getId()).isGreaterThan(0);
    }
}
```
![User-Test](images/2021-10-13_Test_User.jpg)

You can also see it in the terminal:
```
Hibernate: insert into users (email, first_name, last_name, password) values (?, ?, ?, ?)
```

![User-Test2](images/2021-10-13_Test_User2.jpg)

2. Test for listing users in the Database:

```java
    //2.Test: For listing users in the database
    @Test
    public void testListAll(){
        //You can find the findAll-Method in the CrudRepository Interface
        //Klick on the yellow Lamp --> Add local Variable --> Choose users!
        Iterable<User> users = repo.findAll();
        Assertions.assertThat(users).hasSizeGreaterThan(0);

        //Iterrieren über jedes User-Objekt
        //toString-Methode in der User-Klasse implementieren
        for (User user: users) {
            System.out.println(user);
        }
    }
```
![User-Test3](images/2021-10-13_Test_User3.jpg)

3. Test for updating user data in the Database:

**Assertions**

Assertions, wie assert-Anweisungen vereinfachend genannt werden, dienen dazu, **bestimmte Annahmen über den Zustand eines Programms zu verifizieren und sicherzustellen, dass diese eingehalten werden**. Bsp.: Im Programm soll überprüft werden, ob eine Variable x nicht-negativ ist.

```java
    //3.Test: For updating the user
    @Test
    public void testUpdateUser(){
        //Variable für die userID
        Integer userId = 1;
        //findByID refered to the CrudRepository (Typ: Optional<>)
        Optional<User> optionalUser = repo.findById(userId);
        User user = optionalUser.get();
        user.setPassword("hello2000");
        //Save and Update the change into the database
        repo.save(user);

        //For test we can get the user back
        User updatedUser = repo.findById(userId).get();
        //Überprüfung mit Assertions
        //Bestimmte Annahmen über den Zustand des Programmes zu verifizieren und sicherzustellen, dass diese eingehalten werden!
        Assertions.assertThat(updatedUser.getPassword()).isEqualTo("hello2000");
    }
```
![User-Test4](images/2021-10-13_Test_User4.jpg)

4. Test for receiving User by ID

```java
    //4.Test: Receiving User by ID
    @Test
    public void testGet(){
        Integer userId = 2;
        Optional<User> optionalUser = repo.findById(userId);
        Assertions.assertThat(optionalUser).isPresent();
        System.out.println(optionalUser.get());
    }
```

![User-Test5](images/2021-10-13_Test_User5.jpg)

5. Test for deleting User by ID:

```java
    //5.Test: Deleting User by ID
    @Test
    public void testDeleteUserById(){
        Integer userId = 2;
        repo.deleteById(userId);

        Optional<User> optionalUser = repo.findById(userId);
        Assertions.assertThat(optionalUser).isNotPresent();
    }
```
![User-Test6](images/2021-10-13_Test_User6.jpg)

+ Code Users Listing Page

![User-Listing-Page](images/2021-10-10_Erwartetes_Ergebnis_2.jpg)

Create a new Java Class: **UserService**

1. Method to list Users in the Database

```java
package com.mycompany.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//SpringBoot to detect this class
@Service
public class UserService {
    //Referenz zum UserRepository
    @Autowired private UserRepository repo;

    //1.Method: For listing all Users in the Database
    public List<User> listAll(){
        //findAll returns an Iterable Object, so we have to cast the return Type to List!
        return (List<User>)repo.findAll();
    }
}
```
2. Method for showing the User List on the Homepage

Create a new Java Class for the User Model: **UserController**

```java
package com.mycompany.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class UserController {
    //Referenz auf eine Instanz von UserService
    @Autowired private UserService service;

    //2.Method: For showing the User List on the Homepage
    //For handeling the request of User Data
    @GetMapping("/users")
    public String showUserList(Model model){
        List<User> listUsers = service.listAll();
        //Put this List Attribut in the Model Typ model
        model.addAttribute("listUsers",listUsers);
        return "users";
    }
}
```
3. Create a new html file in the templates Directory called **users.html** 

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Manage Users</title>

    <!-- Bootstrap einbinden -->
    <link rel="stylesheet" type="text/css" th:href="@{/webjars/bootstrap/css/bootstrap.min.css}"/>
</head>
<body>
<div class="container-fluid text-center">
    <div><h2>Manage Users</h2></div>
    <!-- Section for adding new User-->
    <div>
        <a class="h3">Add New User</a>
    </div>
    <!-- New Div for the User Listing TABLE-->
    <div>
        <table class="table table-bordered">
            <!-- For the table colomns Name-->
            <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>E-Mail</th>
                <th>First Name</th>
                <th>Last Name</th>
                <!-- Empty Header to the Hyperlinks-->
                <th></th>
            </tr>
            </thead>
            <!-- For the real information data-->
            <!-- We have to use the Thymeleaf Header option-->
            <tbody>
                <th:block th:each="user : ${listUsers}">
                    <tr>
                        <!--Referenz auf die Datenfelder aus der User-Class-->
                        <td>[[${user.id}]]</td>
                        <td>[[${user.email}]]</td>
                        <!-- ACHTUNG: Obwohl als firstName deklariert, wird nur firstname akzeptiert-->
                        <td>[[${user.firstname}]]</td>
                        <td>[[${user.lastname}]]</td>
                        <!-- Empty for the Action Hyperlink: Edit & Delete-->
                        <td>
                            <a class="h4 mr-3">Edit</a>
                            <a class="h4">Delete</a>
                        </td>
                    </tr>
                </th:block>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
```
![User-Page](images/2021-10-13_User_Page.jpg)

4. Specify the Links for the URL of *Add New User* , *Edit* & *Delete*

+ Add New User Function

```html
</head>
<body>
<div class="container-fluid text-center">
    <div><h2>Manage Users</h2></div>
    <!-- Section for adding new User-->
    <div class="m-2">
        <a class="h3" th:href="@{/users/new}">Add New User</a>
    </div>
    <!-- New Div for the User Listing TABLE-->
    <div>
```
+ Edit & Delete Function

```html
                        <!-- ACHTUNG: Obwohl als firstName deklariert, wird nur firstname akzeptiert-->
                        <td>[[${user.firstname}]]</td>
                        <td>[[${user.lastname}]]</td>
                        <!-- For the Action Hyperlink: Edit & Delete-->
                        <td>
                            <a class="h4 mr-3" th:href="@{'/users/edit/' + ${user.id}}">Edit</a>
                            <a class="h4" th:href="@{'/users/delete/'+ ${user.id}}">Delete</a>
                        </td>
                    </tr>
                </th:block>
```

5. Do the 1.Test-Method to test Add New User

```java
   //1.Test: For testing we add a new User
    @Test
    public void testAddNew(){
    }
```
![Test-User-Page](images/2021-10-13_Test_User_Page.jpg)

+ Code Add User Function

![AddUser-Function](images/2021-10-10_Erwartetes_Ergebnis_3.jpg)

1. Update the UserController Class

```java
    //Method: For handeling Add New User
    //Referenz zu users.html
    @GetMapping("/users/new")
    public String showNewForm(Model model){
        model.addAttribute("user",new User());
        return "user_form";
    }
```
2. Add new html file called **user_form.html**

Tipp:

    Kopiere den Inhalt aus der index.html und baue auf diesem auf!

**Update User Entity class User.java** 

```java
    private boolean enabled;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
```
**Check the Database for Update**

![Enabled-Database-Check](images/2021-10-13_Datenfeld_enabled_DatabaseCheck.jpg)

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Add New User</title>

    <!-- Bootstrap einbinden -->
    <link rel="stylesheet" type="text/css" th:href="@{/webjars/bootstrap/css/bootstrap.min.css}"/>
</head>
<body>
<div class="container-fluid">
    <div class="text-center"><h2>Add New User</h2></div>
    <form th:action="@{/users/save/}" method="post" th:object="${user}" style="max-width: 500px;margin: 0 auto;">
        <div class="border border-secondary rounded p-3">
            <div class="form-group row">
                <label class="col-sm-4 col-form-label">E-Mail:</label>
                <div class="col-sm-8">
                    <input type="email" th:field="*{email}" class="form-group"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label">First Name:</label>
                <div class="col-sm-8">
                    <input type="text" th:field="*{firstname}" class="form-group"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label">Last Name:</label>
                <div class="col-sm-8">
                    <input type="text" th:field="*{lastname}" class="form-group"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label">Password:</label>
                <div class="col-sm-8">
                    <input type="password" th:field="*{password}" class="form-group"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label">Enabled:</label>
                <div class="col-sm-8">
                    <input type="checkbox" th:field="*{enabled}" class="form-group"/>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary m-2">Save</button>
                <button type="submit" class="btn btn-secondary m-2">Cancel</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>
```
![Add-New-User](images/2021-10-13_Add_New_User.jpg)

+ Code Form Validation

**Specify some html attributes to validate Data**

      required
      minlength
      maxlenght

**Code the Cancel-Button**

Wichtig: 

    onclick-Funktion ="cancelForm()" einbinden!

```javascript
            <div class="text-center">
                <button type="submit" class="btn btn-primary m-2">Save</button>
                <button type="submit" class="btn btn-secondary m-2"onclick="cancelForm()">Cancel</button>
            </div>
        </div>
    </form>
</div>
//Einbindung von JavaScript für den Cancel-Button
<script type="text/javascript">
    function cancelForm(){
        //Use Thymeleaf expression!
        window.location = "[[@{/users}]]"
    }
</script>
</body>
</html>
```
+ **Implement the method save(user) / saveUser() in the classes UserService and UserController**

**UserService Class**
```java
    public void save(User user) {
        repo.save(user);
    }
}
```
**UserController Class**
```java
    //Method: For save User Data
    @PostMapping("/users/save")
    //User user - because we create an object in the user_form.html
    //<form th:action="@{/users/save}" method="post" th:object="${user}"
    public String saveUser(User user){
        service.save(user);
        return "redirect:/users";
    }
```
**Test the Add New User Function**

![Test-Add-User](images/2021-10-13_Test_Add_New_User_Function.jpg)

**Check the Database for Update**

![Check-Database-Add-User](images/2021-10-13_Test_Add_New_User_Function_Database.jpg)

+ Implement Message User Added on the Mainpage

**UserController Class**

```java
    //Method: For save User Data
    @PostMapping("/users/save")
    //User user - because we create an object in the user_form.html
    //<form th:action="@{/users/save}" method="post" th:object="${user}"
    public String saveUser(User user, RedirectAttributes redirectAttributes){
        service.save(user);
        redirectAttributes.addFlashAttribute("message", "The User has been successfully added!")
        return "redirect:/users";
    }
```
**users.html**

```html
<body>
<div class="container-fluid text-center">
    <div><h2>Manage Users</h2></div>
    <!-- Section for adding new User-->
    <div class="m-2">
        <a class="h3" th:href="@{/users/new}">Add New User</a>
    </div>
    <!-- Implement Message "User Successfully Added"-->
    <div th:if="${message}" class="alert alert-success text-center">
        <!--Displays a Message-->
        [[${message}]]
    </div>
```
**Test the Alert Function**

![Alert-Add-User](images/2021-10-13_Add_User_Alert.jpg)

**Check the Database for Update**

![Check-Database-Add-User](images/2021-10-13_Check_Add_User_Database.jpg)

+ Code Edit / Update User Function

 1. Update UserService Class (implement the get(id):User-Method)

```java
    public User get(Integer id) throws UserNotFoundException {
        Optional<User> result = repo.findById(id);
        if(result.isPresent()){
            return result.get();
        }else{
            throw new UserNotFoundException("Could NOT find any Users with ID: "+id);
        }
    }
```

 2. Update UserController Class (implement the showEditForm()-Method)

```java
    //Method: For editing User Data
    //Use special syntax for the ID
    @GetMapping("/users/edit/{id}")
    public String showEditForm(@PathVariable("id")Integer id,Model model,RedirectAttributes redirectAttributes){
        try {
            User user = service.get(id);
            model.addAttribute("user", user);
            return "user_form";
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", "The User has been successfully saved!");
            return "redirect:/users";
        }
    }
```

 3. Update user_form.html and users.html

**Change Page Title to Thymeleaf expression**

```html
<head>
    <meta charset="UTF-8">
    <!-- Use Thymeleaf expression-->
    <title>[[${pageTitle}]]</title>

    <div class="text-center"><h2>[[${pageTitle}]]</h2></div>
```

**Update UserController**

```java
    public String showNewForm(Model model){
        ...
        model.addAttribute("pageTitle","Add New User");
    }

    public String showEditForm(@PathVariable("id")Integer id,Model model,RedirectAttributes redirectAttributes){
        ...
        model.addAttribute("pageTitle","Edit User with ID: "+id);
    }
```
**Test the Edit Function**

![Edit-User-ID](images/2021-10-13_Edit_User_With_ID.jpg)

**Add Column for Enabled**

Update the users.html File:

```html
<th>Enabled</th>

<td>[[${user.enabled}]]</td>
```
![Column-Enabled](images/2021-10-13_Column_Enabled.jpg)

**Code for Updating User (same as Adding New User)**

Add to the user_form.htmtl:

```html
</head>
<body>
<div class="container-fluid">
    <div class="text-center"><h2>[[${pageTitle}]]</h2></div>
    <form th:action="@{/users/save}" method="post" th:object="${user}" style="max-width: 500px;margin: 0 auto;">
        <!--To Update the User Class-->
        <!-- Map to the User Entity Class ID-->
        <input type="hidden" th:field="*{id}">
```
Test the Update Function:

![Update-Function](images/2021-10-13_Update_Function.jpg)

**Check the Database for Update**

![Check-Update-Database-Edit](images/2021-10-13_Check_Update_Database_Edit.jpg)

**Code Delete User Function**

1. Implement delete(id)-Method in the UserRepository and UserService Class

**Update the UserRepository Class:**

```java
public interface UserRepository extends CrudRepository<User,Integer> {
    //Name of the method is by convention
    public Long countById(Integer id);
}
```

**Update the UserService Class:**

```java
    //Method: For deleting User
    //First check if there is any user with the specified ID
    //Declar in the UserRepository Class
    public void delete(Integer id) throws UserNotFoundException {
        Long count = repo.countById(id);
        if(count == null || count == 0){
            throw new UserNotFoundException("Could NOT find any Users with ID: "+id);
        }else {
            repo.deleteById(id);
        }
    }
```

2. Implement deleteUser()-Method in the UserController Class (ähnlich showEditForm-Methode)

```java
    //Method: For deleting User in the Listing Page
    @GetMapping("/users/delete/{id}")
    public String deleteUser(@PathVariable("id")Integer id,RedirectAttributes redirectAttributes){
        try {
            service.delete(id);
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", e.getMessage());
        }
        return "redirect:/users";
    }
```
**Test the Delete Function:**

![Delete-Function](images/2021-10-13_Delete_Function.jpg)

**Check the Database for Update**

![Delete-Function2](images/2021-10-13_Delete_Function2.jpg)

**Check the Delete Function over URL**

![Delete-Function3](images/2021-10-13_URL_Check.jpg)

### SPRING BOOT FULL STACK DEPARTMENTMANAGEMENT

https://www.youtube.com/watch?v=c3gKseNAs9w

**Spring Boot Framework in Detail**

#### Introduction

+ Basic of the Theory: What is Spring and Spring Boot Framework
+ Create a REST-API using Spring Boot Framwork (GET-Data, DELETE-Data, UPDATE-Data,...)
+ Exception Handling
+ Implement Unit-Testing for all the layers using JUnit (Controller-Layer, Service-Layer, ...)
+ First using the In-Memory-Database, later MySQL-Database (Save and Fetch all this data)
+ Properties-File
+ Implement Matrix for Spring Boot Application
+ Deploy Application to Production
+ See different Ways of Implementation

**Spring = Java Framework to build an Enterprise-Ready-Application (Configurations, Properties,Technolgies, Modules, JAR-Files, Dependencies,...)**

    Spring Framwork makes it easy to create an application, but there is a lot to do in the configuration before!

Examples Modules:

+ Spring Code
+ Spring Web
+ Spring Batch
+ Spring Data
+ Spring API
+ ...


**Spring Boot = Extensions of the Spring Framework (Spring Boot uses the Spring Framework internal)**

    Spring Boot concentrate on the actual work and NOT on the configuration part!

+ Spring Boot provides different **Starter-Templates** that includes all the dependencies (required ones)
+ Spring Boot provides **Auto-Configuration** for all those dependencies and libraries
+ Spring Boot provides Embedded Servers (https://docs.spring.io/spring-boot/docs/2.1.9.RELEASE/reference/html/howto-embedded-web-servers.html) **Creating JAR-File NOT WAR-File**

JAR = Java Archive

WAR = Web Application Ressource / Archive

#### Dependency Injection (Einbringen von Abhängigkeiten)

    Dependecy Injection pattern is the default way creating different Objects in the application ⟶ Kopplung & Kohäsion! We give the control to Spring, if we for example create a new Object (the pattern creats it for us). If we need an Object, Spring will give it to us!

Additional Info:

**Inversion of Control (IOC) = Umkehrung der Steuerung**

Dies bezeichnet ein Umsetzungsparadigma, das u. a. in der objektorientierten Programmierung Anwendung findet.

Dieses Paradigma beschreibt die *Arbeitsweise von Frameworks*: 

Eine Funktion eines Anwendungsprogramms wird bei einer Standardbibliothek registriert und von dieser zu einem späteren Zeitpunkt aufgerufen. Statt dass die Anwendung den Kontrollfluss steuert und lediglich Standardfunktionen benutzt, wird die Steuerung der Ausführung bestimmter Unterprogramme an das Framework abgegeben.

#### Spring Initializr

Create the Barebone Spring Boot Project with:

https://start.spring.io/

Tipp:

    Always go with latest stable Version of Spring Boot (in our case: 2.5.5)

Dependencies:

+ **Spring Web** (Build web, including RESTful, applications using Spring MVC. Uses Apache Tomcat as the default embedded container.)
+ **H2 Database** (Provides a fast in-memory database that supports JDBC API and R2DBC access, with a small (2mb) footprint. Supports embedded and server modes as well as a browser based console application.)

![Spring-Initializr](images/2021-10-17_Spring_Initializr_Tutorial2.jpg)

Click on **Generate**, **Download** the File and Save it on the right place. After that Open your IDE (IntelliJ) and click **Open New Project**. Pick the *pom.xml* and choose trust Project.

![Tutorial2-Start](images/2021-10-17_Tutorial2_Start.jpg)

Das Projekt lässt sich mit jeder IDE öffnen. Zu empfehlen wäre neben IntelliJ noch die IDE **Spring Tool Suite (STS)**

#### Simple API (Application Programming Interface)

Use [STRG + Klick] on @SpringBootApplication to get the Information!

```java
@SpringBootApplication --> @EnableAutoConfiguration

public @interface EnableAutoConfiguration {
    String ENABLED_OVERRIDE_PROPERTY = "spring.boot.enableautoconfiguration";

    Class<?>[] exclude() default {};

    String[] excludeName() default {};
}
```
Unter *exclude* besteht die Möglichkeit gewisse Dependencies auszuschließen!

+ Erstelle im *main-Package* ein neues Package *controller* und in diesem eine neue Klasse **HelloController.java**
+ HelloController = Ressource
+ Damit die Klasse in den Spring-Container übernommen wird, müssen wir noch die Annotation **@Component** hinzufügen

```java
package com.itkolleg.SpringBoot.Tutorial.controller;

import org.springframework.stereotype.Component;

@Component
public class HelloController {
    
}
```
**Weitere Variante für Simple Controller: Verwendung der Annotation @Controller , diese beinhaltet bereits die Annotation @Component by default**

```java 
package com.itkolleg.SpringBoot.Tutorial.controller;

import org.springframework.stereotype.Controller;

@Controller
public class HelloController {

}
```
Use [STRG + Klick] on @Controller to get the Information!

```java
package org.springframework.stereotype;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.core.annotation.AliasFor;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Controller {
    @AliasFor(
        annotation = Component.class
    )
    String value() default "";
}
```
Bessere Variante in unserem Fall für die REST-API: **Verwendung der Annotation @RestController**

```java
package com.itkolleg.SpringBoot.Tutorial.controller;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

}
```
**RestController defines two Things**:

+ Tells that i am a Controller for REST APIs and includes the Controller and Component Annotation
+ Always returns a ResponseBody

```java
package org.springframework.web.bind.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Controller;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Controller
@ResponseBody
public @interface RestController {
    @AliasFor(
        annotation = Controller.class
    )
    String value() default "";
}
```
We want to execute this Method at the Endpoint, so we have to use the Annotation **@RequestMapping**. Inside this we will use the **GET-REQUEST-METHOD**

```java
package com.itkolleg.SpringBoot.Tutorial.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    //Annotation to always execute the method at the Endpoint
    //GET-REQUEST
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String helloWordl(){
        return "Welcome to Daily Code Buffer";
    }
}
```
**Change Configuration: Port**

If you like to change the Port of your Web-Server which IntelliJ uses, you have to make implementation in the file: **application.properties**

![Change-Port](images/2021-10-17_Port_Change.jpg)

Change the Annotation @RequestMapping to **@GetMapping**. Bessere Variante, da @GetMapping bereits @RequestMapping enthält by default (siehe Info @GetMapping)

```java
@RestController
public class HelloController {

    @GetMapping(value = "/")
    public String helloWorld(){
        return "Welcome to Daily Code Buffer";
    }
}
```
**Programm über das Terminal/Konsole starten**

```
mvn spring-boot:run
```
#### Add Spring Boot Devtools

    With this dependency, we dont have to Start and Stop our application every time to view changes.

Go to https://start.spring.io/ and select Add Dependencies. Then choose **Spring Boot Devtools**. Click on Explore to view the json-file. Copy the dependency into your *pom.xml*-File

![SpringIO-Dependencies](images/2021-10-17_SpringIO_Dependencies.jpg)

```json
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-devtools</artifactId>
      <scope>runtime</scope>
      <optional>true</optional>
    </dependency>
```
Next step is to make some changes in the Settings of IntelliJ

![Devtools-IntelliJ-Settings2](images/2021-10-17_IntelliJ_Build_Project_Automatically.jpg)

![Devtools-IntelliJ-Settings2](images/2021-10-10_DevTools4.jpg)

#### Architecture and Examples

![Architecture1](images/2021-10-17_Diagramm_API.jpg)

![Architecture2](images/2021-10-17_Diagramm_API2.jpg)

+ Adding different REST-APIs for the Department-Entity (Implementing in the **DepartmentController** Class). REST-API = Controller Layer (Request & Response)
+ Creating Service Layer = Business Layer (all the Business Logic we want to add)
+ Data Access/Repository Layer responsible to interact with our Database. This Layer will handle all database operations! For this Layer we will use the **Spring Data JPA**
+ Create a Database (**First: H2 (In-Memory-Databse) | Later: MySQL**). We will learn how to change the database using Configurations!

#### H2 and JPA Dependency

**Spring Data JPA**

Persist data in SQL stores with Java Persistence API using Spring Data and Hibernate.

Add the dependency over https://start.spring.io/

```json
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
```

Add Configuration-Details in the **application.properties**

```sql
#spring.h2.console.enabled=true
#spring.datasource.url=jdbc:h2:mem:dcbapp
#spring.datasource.driverClassName=org.h2.Driver
#spring.datasource.username=root
#spring.datasource.password=123
#spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
```

**Zugriff auf die H2-Database**

    localhost:8082/h2-console

![H2-Console](images/2021-10-17_H2_Console.jpg)

#### Creating Components

Start with creating different Packages

+ entity (Here create: Department.java)
+ service
+ repository

**Important**

    @Entity = Important Annotation to make a class an entity and to interact with the database

    @Id and @GeneratedValue (strategy = GenerationType.AUTO) are important Annotations for setting a Primary Key!

+ Create the **DepartmentController.java** in the *controller package*

```java
package com.itkolleg.SpringBoot.Tutorial.controller;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartmentController {
    
}
```
+ Create the **DepartmentService.java Interface** in the *service package*
+ Create the **DepartmentServiceImplementation.java** in the *service package* 

```java
package com.itkolleg.SpringBoot.Tutorial.service;

import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceImplementation implements DepartmentService {

}
```
+ Create the **DepartmentRepository.java Interface** in the *repository package* 

```java
package com.itkolleg.SpringBoot.Tutorial.repository;

import com.itkolleg.SpringBoot.Tutorial.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository  extends JpaRepository<Department, Long> {
}
```
![Structure1](images/2021-10-17_Tutorial2_Structure1.jpg)

#### Department Save API

**Create the Methods for the DepartmentController.java**

+ saveDepartment

**JSON Object to Department Entity with @RequestBody** (Hier erleichtert uns Spring die Entwicklung und spart uns Zeit an zusätzlicher Implementierung)

With the Annotation **@Autowired** you create a Reference to the needed Class

**Autoimplement by IntelliJ the method *saveDepartment* in the interface DepartmentService** 

```java
public Department saveDepartment(Department department);
```

**...and also in the DepartmentServiceImplementation.java!**

```java
public class DepartmentServiceImplementation implements DepartmentService {

    @Override
    public Department saveDepartment(Department department) {
        return null;
    }
}
```
**saveDepartment-Method**

```java
package com.itkolleg.SpringBoot.Tutorial.service;

import com.itkolleg.SpringBoot.Tutorial.entity.Department;
import com.itkolleg.SpringBoot.Tutorial.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceImplementation implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    /**
     * This Method saves the data of a JSON-File
     * in our database (Enitity Department)
     * @param department
     * @return
     */
    @Override
    public Department saveDepartment(Department department) {
        //A lot of Methods from the JPA Repository we have extended
        return departmentRepository.save(department);
    }
}
```
**Spring-Data will also create the tables for us**

#### Testing APIs using REST CLIENT

You can use: **Postman, Insomnia, VS-CODE, ...**

+ Check the database over **localhost:8082/h2-console**

![Tables-Database](images/2021-10-18_Tables_Database.jpg)

+ Test in Insomnia

**POST-Request erstellen**: Save Department

POST https://localhost:8082/departments

In the body choose **JSON-Data** and declare the json code:

```json
{
	"departmentName":"IT",
	"departmentAddress":"Innsbruck",
	"departmentCode":"IT-07"
}
```
![Insomnia](images/2021-10-18_Insomnia.jpg)

Hinweis:

    Die Daten die wir übertragen werden nicht gespeichert, nach einem Neustart sind die bereits erstellten Daten nicht mehr vorhanden!

#### GetMapping - Fetching data from Database

**Create the Method: *fetchDepartmentList* in the DepartmentController Class to get all the Departments from the Database**

```java
    @GetMapping("/departments")
    public List<Department> fetchDepartmentList(){
        return departmentService.fetchDepartmentList();
    }
```
+ You have to import the Method for the DepartmentService Interface

```java
public interface DepartmentService {

    public Department saveDepartment(Department department);

    public List<Department> fetchDepartmentList();
}
```
+ You have to import the Method for the  DepartmentServiceImplementation Class

```java
    @Override
    public List<Department> fetchDepartmentList() {
        return departmentRepository.findAll();
    }
```
+ Make new POST Requests over Insomnia and create a New Request *Get Department* (Copy the URL from the POST Request)

![GET-Request](images/2021-10-19_GET_Request.jpg)

#### Fetching Data by ID

**Create a Method: *fetchDepartmentById* in the DepartmentController Class to get all data by ID**

Tipp:
```java
{id} = Path Variable
@GetMapping("/departments/{id}")
//Die eckigen Klammern um id stehen für einen dynamic Value
//D.h. egal welche ID wir bekommen, behandeln wir Sie als Department ID!
```
+ You have to import the Method for the DepartmentService Interface

```java
public Department fetchDepartmentById(Long departmentId);
```
+ You have to import the Method for the  DepartmentServiceImplementation Class

```java
    @Override
    public Department fetchDepartmentById(Long departmentId) {
        return departmentRepository.findById(departmentId).get();
    }
```
+ Use the Save Department in Insomnia and make some POST Requests. After that make a GET Request to see the Data we sent. Now change the URL to: **http://localhost:8082/departments/2**

![GET-Request2](images/2021-10-19_GET_Request2.jpg)

#### Deleting Data

**Create a Method: *deleteDepartmentById* in the DepartmentController Class to delete a department by ID**

```java
    @DeleteMapping("/departments/{id}")
    public String deleteDepartmentById(@PathVariable("id") Long departmentId){
        departmentService.deleteDepartmentById(departmentId);
        return "Department deleted Successfully!";
    }
```
+ You have to import the Method for the DepartmentService Interface

```java
 public void deleteDepartmentById(Long departmentId);
```
+ You have to import the Method for the  DepartmentServiceImplementation Class

```java
    @Override
    public void deleteDepartmentById(Long departmentId) {
        departmentRepository.deleteById(departmentId);
    }
```
+ Use the Save Department in Insomnia and make some POST Requests. After that make a GET Request to see the Data we sent. Change the URL to: **http://localhost:8082/departments/**

+ Create a New Request *Delete Department*. Use the URL: http://localhost:8082/departments/2

![DELETE-Request](images/2021-10-19_DELETE_Request.jpg)

#### Updating Data

**Create a Method: *updateDepartment* in the DepartmentController Class to delete a department by ID**

```java
    @PutMapping("/departments/{id}")
    public Department updateDepartment(@PathVariable("id") Long departmentId, @RequestBody Department department){
        return departmentService.updateDepartment(departmentId, department);
    }
```
+ You have to import the Method for the DepartmentService Interface

```java
    public Department updateDepartment(Long departmentId, Department department);
```

+ You have to import the Method for the  DepartmentServiceImplementation Class

```java
    @Override
    public Department updateDepartment(Long departmentId, Department department) {
        Department departmentDatabase = departmentRepository.findById(departmentId).get();
        if(Objects.nonNull(department.getDepartmentName())&& !"".equalsIgnoreCase(department.getDepartmentName())){
            departmentDatabase.setDepartmentName(department.getDepartmentName());
        }
        if(Objects.nonNull(department.getDepartmentAddress())&& !"".equalsIgnoreCase(department.getDepartmentAddress())){
            departmentDatabase.setDepartmentAddress(department.getDepartmentAddress());
        }
        if(Objects.nonNull(department.getDepartmentCode())&& !"".equalsIgnoreCase(department.getDepartmentCode())){
            departmentDatabase.setDepartmentCode(department.getDepartmentCode());
        }
        return departmentRepository.save(departmentDatabase);
    }
```
+ Use the Save Department in Insomnia and make some POST Requests. After that make a GET Request to see the Data we sent. Change the URL to: **http://localhost:8082/departments/**

+ Create a New Request *Update Department*. Use the URL: http://localhost:8082/departments/2

![UPDATE-Request](images/2021-10-19_UPDATE_Request.jpg)

#### Fetch Data by Name 

**Create a Method: *fetchDepartmentByName* in the DepartmentController Class to get a department by Name**

```java
    @GetMapping("/departments/name/{name}")
    public Department fetchDepartmentByName(@PathVariable("name") String departmentName){
        return departmentService.fetchDepartmentByName(departmentName);
    }
```
+ You have to import the Method for the DepartmentService Interface

```java
    public Department fetchDepartmentByName(String departmentName);
```

+ You have to import the Method for the  DepartmentServiceImplementation Class

```java
    @Override
    public Department fetchDepartmentByName(String departmentName) {
        return departmentRepository.findByDepartmentName(departmentName);
    }
```
+ You have to create the Method *fetchDepartmentByName* in the DepartmentRepsoitory Class

```java
    public Department findByDepartmentName(String departmentName);
```
+ Use the Save Department in Insomnia and make some POST Requests. After that make a GET Request to see the Data we sent. Change the URL to: **http://localhost:8082/departments/**

+ Create a New Request *Department By Name*. Use the URL: http://localhost:8082/departments/name/CE/

**ACHTUNG**

    Exakte Angabe des Namens, "ce" z.B. wird nicht gefunden!

![GET-Request2](images/2021-10-20_GET_Request2.jpg)

+ To find the exact Value, implement the Method: *findByDepartmentNameIgnoreCase* in the Department Repository Interface

```java
    public Department findByDepartmentNameIgnoreCase(String departmentName);

```
+ Use the method *findByDepartmentNameIgnoreCase* instead of *findByDepartmentName* in the DepartmentServiceImplementation Class
```java
    @Override
    public Department fetchDepartmentByName(String departmentName) {
        //return departmentRepository.findByDepartmentName(departmentName);
        return departmentRepository.findByDepartmentNameIgnoreCase(departmentName);
    }
```
![IgnoreCase](images/2021-10-20_IgnorseCase_JPA.jpg)

**HINWEIS**
```
To implement different JPA-Methods, you can change the name of a method to a special declaration:

findByDepartmentName ⟶ findByDepartmentNameIgnoreCase
```
**JPA Query Documentation**

https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods

![JPA-Doku](images/2021-10-20_JPA_Doc.jpg)

In and NotIn also take any subclass of Collection as a parameter as well as arrays or varargs. For other syntactical versions of the same logical operator, check “Repository query keywords” 

https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repository-query-keywords 

**Annotation-based Configuration**

Annotation-based configuration has the advantage of not needing another configuration file to be edited, lowering maintenance effort. **You pay for that benefit by the need to recompile your domain class for every new query declaration.**

Example: Annotation-based named query configuration

```java
@Entity
@NamedQuery(name = "User.findByEmailAddress",
  query = "select u from User u where u.emailAddress = ?1")
public class User {

}
```
**Using @Query Configuration**

Using named queries to declare queries for entities is a valid approach and works fine for a small number of queries. As the queries themselves are tied to the Java method that runs them, you can actually bind them directly by using the Spring Data JPA @Query annotation rather than annotating them to the domain class. This frees the domain class from persistence specific information and co-locates the query to the repository interface.

Queries annotated to the query method take precedence over queries defined using @NamedQuery or named queries declared in orm.xml.

Example: Declare query at the query method using @Query

```java
public interface UserRepository extends JpaRepository<User, Long> {

  @Query("select u from User u where u.emailAddress = ?1")
  User findByEmailAddress(String emailAddress);
}
```
#### HIBERNATE Validation

**Add the Validation for all the APIs we created (Inputs from save the data, update the data). For all this we can use the HIBERNATE VALIDATION**

+  Add the dependencies for Hibernate

```json
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-validation</artifactId>
		</dependency>
```
**The spring-boot-starter-validation uses the hibernate validator**

![Hibernate-Starter-Validation](images/2021-10-20_Hibernate_Starter_Validation.jpg)

+ Annotate our Entities to validate all of our Requests in the Department Class

**departmentName should be required (departmentAddress and departmentCode are optional)**

```java
    @NotBlank(message = "Please Add Department Name!")
    private String departmentName;
```

+ Add @Valid to the method saveDepartment in the DepartmentController Class

**The @Valid annotation ensures the validation of the whole object. Importantly, it performs the validation of the whole object graphs.**

```java
    public Department saveDepartment(@Valid @RequestBody Department department)
```

+ Test the Validation Function

![Test-Validate1](images/2021-10-20_Test_Validate1.jpg)

+ Different Validation Types

```java
@Length(max = 10,min = 0)    
@Size(max = 10, min = 0)
@Email
@Positive
@Negative
@PositiveOrZero
@NegativeOrZero
@Future
@FutureOrPresent
@Past
@PastOrPresent
```
#### Adding Loggers 

At this moment we are adding Simple Loggers, but this can be extended to Special Loggers (z.B.: File Loggers). Spring Boot comes with the **SLF4J Library**. They are directly implemented and we can directly use it (NO dependency import needed for this Logger)

    Logger are helpful for Debugging!

+ Initialize the Logger in the DepartmentController Class

```java
    public  final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);
```
+ Add the Logger to the Methods *saveDepartment* and *fetchDepartmentList*

```java
    public Department saveDepartment(@Valid @RequestBody Department department){
        LOGGER.info("Inside saveDepartment of DepartmentController");
        return departmentService.saveDepartment(department);
    }
```
```java
    public List<Department> fetchDepartmentList(){
        LOGGER.info("Inside fetchDepartmentList of DepartmentController");
        return departmentService.fetchDepartmentList();
    }
```
+ Test the Logger Implementation 

![Logger](images/2021-10-20_Logger.jpg)

#### Project Lombok (Removing Boiler plate code)

Project Lombok is a java library that automatically plugs into your editor and build tools, spicing up your java.
Never write another getter or equals method again, with one annotation your class has a fully featured builder, Automate your logging variables, and much more.

+ Add the dependency and the plugin to our pom.xml File

```
    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
      <optional>true</optional>
    </dependency>
```
```
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<excludes>
						<exclude>
							<groupId>org.projectlombok</groupId>
							<artifactId>lombok</artifactId>
						</exclude>
					</excludes>
				</configuration>
			</plugin>
```

+ **Remove** all Constructors, Getter-Setter and the to-String-Method from the Department Class
+ Add the **@Data** (Lombok) Annotation in the *Department*-Class

```java
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Department {
}
```
#### Exception Handling

+ Create the Error-Handling in the DepartmentController Class in the Method *fetchDepartmentById*

+ Create a new Package called *errors* and a new class *DepartmentNotFoundException* inside this package
  
+ Change the Method *fetchDepartmentById* in the DepartmentServiceImplementation Class

```java
    @Override
    public Department fetchDepartmentById(Long departmentId) throws DepartmentNotFoundException {
       Optional<Department> department = departmentRepository.findById(departmentId);
       if(!department.isPresent()){
           throw new DepartmentNotFoundException("Deparment Not Available!");
       }
       return department.get();
    }
```
+ Create a new Class *RestResponseEntityExceptionHandler* in the error-Package

```java
package com.itkolleg.SpringBoot.Tutorial.error;

import com.itkolleg.SpringBoot.Tutorial.entity.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@ResponseStatus
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(DepartmentNotFoundException.class)
    public ResponseEntity<ErrorMessage> departmentNotFoundException(DepartmentNotFoundException exception, WebRequest request){

        ErrorMessage message = new ErrorMessage(HttpStatus.NOT_FOUND, exception.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
    }
}
```
+ Create a new Class *ErrorMessage* in the entity-Package

```java
package com.itkolleg.SpringBoot.Tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorMessage {

    private HttpStatus status;
    private String message;
}
```
![Exception1](images/2021-10-27_Exception1.jpg)

#### Changing Database: H2 ⟶ MySQL

+ Install MySQL Database or MySQL-Workbench on your machine. You can also use the Database in IntelliJ
+ Change the *application.properties*-File

```
server.port=8888

spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://localhost:3306/dcbapp
spring.datasource.username=root
spring.datasource.password=123
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.jpa.show-sql=true
```
+ Add the mysql-Driver in the *pom.xml*-File

```json
    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <scope>runtime</scope>
    </dependency>
```

![Workbench1](images/2021-10-27_Workbench1.jpg)

Hinweis

    Port ändern auf 8888, da 8082 für den phpMyAdmin im docker-compose File verwendet wird!

![Workbench2](images/2021-10-27_Workbench2.jpg)

#### UNIT Testing

**Every Application should do UNIT-Testing (= Integration Testing)**

We added the dependencies at the beginning:

```
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
```
This Starter-Test implements the **junit-jupiter** and the **mockito-core** for testing!

**Mocking**

Mocking is a process used in unit testing when the unit being tested has external dependencies. The purpose of mocking is to isolate and focus on the code being tested and not on the behavior or state of external dependencies

Test all Layers:
+ Service Layer
+ Repository Layer
+ Controller Layer

##### UNIT Testing Service Layer

+ Change to the *DepartmentService* Interface and Right-Click on the name, choose **Generate** and then **Test**

![Service-Test1](images/2021-10-27_Service_Test1.jpg)

+ Implememt first Method for Testing

    Always use unique and long names for the Test Methods!

```java
package com.itkolleg.SpringBoot.Tutorial.service;

import com.itkolleg.SpringBoot.Tutorial.entity.Department;
import com.itkolleg.SpringBoot.Tutorial.repository.DepartmentRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;

//Important Annotation telling SpringBoot this is a Test-Class
@SpringBootTest
class DepartmentServiceTest {

    @Autowired
    private DepartmentService departmentService;

    //Annotation for Mocking
    @MockBean
    private DepartmentRepository departmentRepository;

    @BeforeEach
    //@BeforeAll
    void setUp() {
        Department department = Department.builder()
                .departmentName("IT")
                .departmentAddress("Innsbruck")
                .departmentCode("IT-06")
                .departmentId(1L)
                .build();

        Mockito.when(departmentRepository.findByDepartmentNameIgnoreCase("IT"))
                .thenReturn(department);
    }

    @Test
    @DisplayName("Get Data based Valid Department Name")
    //@Disabled Annotation if you want to skip a Method for Testing
    public void whenValidDepartmentName_thenDepartmentShouldFound(){
        String departmentName = "IT";
        Department found = departmentService.fetchDepartmentByName(departmentName);

        assertEquals(departmentName, found.getDepartmentName());
    }
}
```
![Service-Test2](images/2021-10-27_Service_Test2.jpg)

##### UNIT Testing Repository Layer

+ Change to the *DepartmentRepository* Interface and Right-Click on the name, choose **Generate** and then **Test**

```java
package com.itkolleg.SpringBoot.Tutorial.repository;

import com.itkolleg.SpringBoot.Tutorial.entity.Department;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class DepartmentRepositoryTest {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void setUp() {
        Department department = Department.builder()
                .departmentName("Mechanical Engineering")
                .departmentCode("ME - 011")
                .departmentAddress("Delhi")
                .build();

        entityManager.persist(department);
    }
    @Test
    public void whenFindById_thenReturnDepartment(){
        Department department = departmentRepository.findById(1L).get();
        assertEquals(department.getDepartmentName(), "Mechanical Engineering");
    }
}
```
![Repository-Test1](images/2021-10-27_Repo_Test1.jpg)

##### UNIT Testing Controller Layer

+ Change to the *DepartmentController* Class and Right-Click on the name, choose **Generate** and then **Test**

1. saveDepartment

![Controller-Test1](images/2021-10-27_Controller_Test1.jpg)

+ Use static import for **MockMvcResultMatchers**

![Controller-Test2](images/2021-10-27_Controller_Test2.jpg)

```java
package com.itkolleg.SpringBoot.Tutorial.controller;

import com.itkolleg.SpringBoot.Tutorial.entity.Department;
import com.itkolleg.SpringBoot.Tutorial.service.DepartmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(DepartmentController.class)
class DepartmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DepartmentService departmentService;

    private Department department;

    @BeforeEach
    void setUp() {
        department = Department.builder()
                .departmentAddress("Innsbruck")
                .departmentCode("IT-06")
                .departmentName("IT")
                .departmentId(1L)
                .build();
    }
    @Test
    void saveDepartment() throws Exception {
       Department inputDepartment = Department.builder()
                .departmentAddress("Innsbruck")
                .departmentCode("IT-06")
                .departmentName("IT")
                .build();

        Mockito.when(departmentService.saveDepartment(inputDepartment))
                .thenReturn(department);

        mockMvc.perform(post("/departments")
                .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"departmentName\":\"IT\",\n" +
                                "\t\"departmentAddress\":\"Innsbruck\",\n" +
                                "\t\"departmentCode\":\"IT-06\"\n" +
                                "}"))
                .andExpect(status().isOk());
    }
```
+ Try saveDepartment-Test

![Controller-Test3](images/2021-10-27_Controller_Test3.jpg)

2. fetchDepartmentById

```java
    @Test
    void fetchDepartmentById() throws Exception {
        Mockito.when(departmentService.fetchDepartmentById(1L))
                .thenReturn(department);
        //GET-Operation
        mockMvc.perform(get("/departments/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.departmentName").value(department.getDepartmentName()));
    }
```
+ Try fetchDepartmentById-Test

![Controller-Test4](images/2021-10-27_Controller_Test4.jpg)

#### Adding Config in Properties-File

+ Implement a Welcome Message in the *application.properties-File*

```
welcome.message = Welcome to Daily Code Buffer by Tom!
```
+ Change the HelloController Class

```java
@RestController
public class HelloController {

    @Value("${welcome.message}")
    private String welcomeMessage;

    @GetMapping(value = "/")
    public String helloWorld(){
        return welcomeMessage;
    }
}
```
![Application-Properties](images/2021-10-27_Application_Properties.jpg)

#### Adding application.yml

**yml-File is more human readable and reduces the duplicate values**

+ Create a new File **application.yml** in the *ressources* Package where you find the *application.properties-File*

+ Convert the data of the application.properties-File with an Online Converter or you can install a Plugin for IntelliJ
  
+ Comment the whole *application.properties*-File out, we are **not using** it anymore!

**appliaction.yml**

```yml
server:
  port: 8888
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/dcbapp
    username: root
    password: 123
  jpa:
    hibernate:
      ddl-auto: update
    show-sql: true
welcome:
  message: Welcome to Daily Code Buffer by Tom!
```
![Application-YML](images/2021-10-27_Application_YML.jpg)

#### Springboot Profiles

**For all the different Environments we are using different configuration properties. For this we are going to use Spring Profiles**

Hinweis

    With --- you can create a different Profiles / Sections in one File!


```yml
server:
  port: 8888

spring:
  profiles:
    active: qa

---

spring:
  profiles: dev
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/dcbapp
    username: root
    password: 123
  jpa:
    hibernate:
      ddl-auto: update
    show-sql: true
welcome:
  message: Welcome to Daily Code Buffer by Tom!

---

spring:
  profiles: qa
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/dcbapp-qa
    username: root
    password: 123
  jpa:
    hibernate:
      ddl-auto: update
    show-sql: true
welcome:
  message: Welcome to Daily Code Buffer by Tom!

---

spring:
  profiles: prod
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/dcbapp-prod
    username: root
    password: 123
  jpa:
    hibernate:
      ddl-auto: update
    show-sql: true
welcome:
  message: Welcome to Daily Code Buffer by Tom!
```

![Spring-Profile1](images/2021-10-28_Spring-Profile1.jpg)

#### Running SpringBoot with multiple Profiles

**Create our JAR-FILES**

+ Open the *pom.xml-File* and change the version 

```xml
	</parent>
	<groupId>com.itkolleg</groupId>
	<artifactId>Spring-Boot-Tutorial</artifactId>
	<version>1.0.0</version>
	<name>Spring-Boot-Tutorial</name>
	<description>Demo project for Spring Boot</description>
	<properties>
		<java.version>11</java.version>
	</properties>
```

Hinweis
```
Damit ich das JAR File über das Terminal generieren lassen kann, muss ich zunächst mit "mvn --v" überprüfen ob die lokale MAVEN Installation die selbe Runtime verwendet wie die Application.
```
Vorgehensweise für Linux Ubuntu 20.04 LTS:

1. sudo nano /etc/profile.d/maven.sh
2. Update the following content to file:
```
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export M2_HOME=/usr/local/maven
export MAVEN_HOME=/usr/local/maven
export PATH=${M2_HOME}/bin:${PATH}
```
Save your file and close.

3. Next load the environment variables in current shell using following command:
```
source /etc/profile.d/maven.sh
```
4. Verify Installation:
```
mvn --v
```

+ Open the Terminal in IntelliJ and create the **JAR-File**

```
mvn clean install
```
![JAR-File1](images/2021-10-28_JAR-File1.jpg)

**To Run this JAR-File with a special profile, you can use following command (without the arguments at the end of the command, the application will be run with the defined active profile "qa" in the application.yml-File)**

```
 java -jar Spring-Boot-Tutorial-1.0.0.jar --spring.profiles.active=prod
```

![Spring-Profile2](images/2021-10-28_Spring-Profile2.jpg)

#### SpringBoot Actuator

**An actuator is a manufacturing term that refers to a mechanical device for moving or controlling something. Actuators can generate a large amount of motion from a small change.**

The spring-boot-actuator module provides all of Spring Boot’s production-ready features. The recommended way to enable the features is to add a dependency on the spring-boot-starter-actuator ‘Starter’.

To add the actuator to a Maven based project, add the following ‘Starter’ dependency:

```xml
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
```
+ Configure the Endpoints in the *application.yml*

Hinweis

    Bei der Verwendung der docker-compose.yml, muss für die Verwendung des Actuator, der Apache Web-Server auf Port 80 auskommentiert werden!

```yml
spring:
  profiles: qa
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/dcbapp-qa
    username: root
    password: 123
  jpa:
    hibernate:
      ddl-auto: update
    show-sql: true
welcome:
  message: Welcome to Daily Code Buffer by Tom!
# Managing (Enable) Endpoints
management:
  endpoints:
    web:
      exposure:
        include: "*"
```

![Actuator1](images/2021-10-29_Actuator.jpg)

#### Custom Actuator Endpoint

+ Create a new package called *config*
+ Inside of this package, create a new Java Class *FeatureEndpoint* 
+ Annotate this class with @Component and @Endpoint(id = "features")

```java
package com.itkolleg.SpringBoot.Tutorial.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Endpoint(id = "features")
public class FeatureEndpoint {

    private final Map<String,Feature> featureMap = new ConcurrentHashMap<>();

    public FeatureEndpoint() {
        featureMap.put("Department",new Feature(true));
        featureMap.put("User",new Feature(false));
        featureMap.put("Authentication",new Feature(false));
    }

    @ReadOperation
    public Map<String, Feature> features(){
        return featureMap;
    }

    @ReadOperation
    public Feature feature(@Selector String featureName){
        return featureMap.get(featureName);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private static class Feature {
        private boolean isEnabled;
    }
}
```
![Feature-Endpoint](images/2021-10-29_Feature_Endpoint.jpg)

#### Exclude Actuator Endpoint

+ Change into the *application.yml* and add the *exclude* Parameter (We exclude env and beans)

```yml
# Managing (Enable) Endpoints
management:
  endpoints:
    web:
      exposure:
        include: "*"
        exclude: "env,beans"
```
![Exclude-Features](images/2021-10-29_Exclude_Features.jpg)

### SPRING DATA JPA VERTIEFUNG

https://www.youtube.com/watch?v=XszpXoII9Sg

This Spring Boot JPA tutorial will teach you how to use Spring Data JPA to create scalable backend apps supported by any relational database. Spring Data JPA is an excellent choice because it allows you to focus on business logic while speeding up your development.

**Spring Data JPA provides repository support for the Java Persistence API (JPA). It eases development of applications that need to access JPA data sources**

**ORM = Object Relationship Mapping**

    Object = Table

#### What we Build

![Klassendiagramm-Tutorial3](images/2021-10-29_Klassendiagramm_Tutorial3.jpg)

#### Connecting SpringBoot App with Database

+ Use https://start.spring.io/ to create a new Spring Application

**Dependencies**

+ Spring Data JPA (Persist data in SQL stores with Java Persistence API using Spring Data and Hibernate.)
+ Lombok (Java annotation library which helps to reduce boilerplate code.)
+ MySQL Driver (MySQL JDBC and R2DBC driver.)
+ Spring Web (Build web, including RESTful, applications using Spring MVC. Uses Apache Tomcat as the default embedded container.)

![Spring-Initializr3](images/2021-10-29_Spring_Intitializr3.jpg)

+ Generate the project and import it to IntelliJ

![Tutorial3](images/2021-10-29_Tutorial3.jpg)

**We are using JPA with the Hibernate Implementation** (If we want we can change it!)

+ Create a new Schema called *schooldb* in MySQL-Workbench
  
+ Add the properties for the database in the *application.properties*-File

```yml
# Port Änderung aufgrund der docker-compose Datei (By Defaul wird 8080 verwendet, dieser ist aber über docker bereits belegt!)
server.port=8888
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://localhost:3306/schooldb
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.jpa.show-sql=true
spring.datasource.username=root
spring.datasource.password=123
```

![Workbench-Properties](images/2021-10-29_Workbench-Properties.jpg)

#### Mapping Entities with Database

+ Create a new package called *entity*
+ Inside of this package, create a new Java Class *Student*

```java
package com.itkolleg.spring.data.jpa.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Student {

    @Id
    private Long studentId;
    private String firstName;
    private String lastName;
    private String emailId;
    private String guardianName;
    private String guardianEmail;
    private String guardianMobile;
}
```

![Entity-Student](images/2021-10-29_Enitity_Student.jpg)

#### Different JPA Annotations
```js
@Table(name = "tbl_student")

@Column(name = "email_address",
        nullable = false
    )
@SequenceGenerator(
            name = "student_sequence",
            sequenceName = "student_sequence",
            allocationSize = 1
    )
@GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "student_sequence"
    )
@Table(name = "tbl_student",
       uniqueConstraints = @UniqueConstraint(
               name = "emailid_unique",
               columnNames = "email_address"
       )
)
```
![TBL-Student](images/2021-10-29_TBL_Student.jpg)

#### Understanding Repositories and Methods

+ Create a new package called *repository*
+ Inside of the package, create a new Interface *StudentRepository*

```java
package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {
    
}
```
+ Generate the Test-Class for StudentRepository by using IntelliJ Generate-Function

+ Use the Annotation @DataJpaTest if you NOT want to store the Data in the Database
+ Create the method *saveStudent* inside of the StudentRepositoryTest Class

```java
package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.Student;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
//@DataJpaTest = If you not want to store the Data in the Database
class StudentRepositoryTest {

    @Autowired
    private StudentRepository studentRepository;

    @Test
    public void saveStudent(){
        Student student = Student.builder()
                .emailId("tomislav@gmail.com")
                .firstName("Tomislav")
                .lastName("Kutz")
                .guardianName("Nikhil")
                .guardianEmail("nikhil@gmail.com")
                .guardianMobile("9999999999")
                .build(); //To build this particular Object!

        studentRepository.save(student);
    }
}
```

![Save-Student](images/2021-10-29_SaveStudent.jpg)

+ Create the method *printAllStudent* inside of the StudentRepositoryTest Class

```java
    @Test
    public void printAllStudent(){
        List<Student> studentList = studentRepository.findAll();
        System.out.println("Studentenliste: "+studentList);
    }
```

![Print-All-Student](images/2021-10-29_PrintAllStudent.jpg)

#### @Embeddable and @Embedded

**@Embeddable**

JPA provides the @Embeddable annotation to declare that a class will be embedded by other entities.

**@Embedded**

The JPA annotation @Embedded is used to embed a type into another entity.

+ Remove the Guardian Attributs of the *Student*-Class and add a data-field
  
```java
    @Embedded
    private Guardian guardian;
```

+ Create a new Java Class called *Guardian*

```java
package com.itkolleg.spring.data.jpa.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@AttributeOverrides({
        @AttributeOverride(
                name = "name",
                column = @Column(name = "guardian_name")
        ),
        @AttributeOverride(
                name = "email",
                column = @Column(name = "guardian_email")
        ),
        @AttributeOverride(
                name = "mobile",
                column = @Column(name = "guardian_mobile")
        ),
})
public class Guardian {

    private String name;
    private String email;
    private String mobile;
}
```
+ Create a new method *saveStudentWithGuardian* inside of the StudentRepositoryTest Class

```java
    @Test
    public void saveStudentWithGuardian(){
        Guardian guardian = Guardian.builder()
                .name("Nikhil")
                .email("nikhil@gmail.com")
                .mobile("9999999999")
                .build();

        Student student = Student.builder()
                .firstName("Philipp")
                .emailId("phil@gmail.com")
                .lastName("Maar")
                .guardian(guardian)
                .build();

        studentRepository.save(student);
    }
```

![SaveStudentWithGuardian](images/2021-10-29._SaveStudentWithGuardian.jpg)

#### Creating JPA Repositories and Methods

+ Change in the *StudentRepository*-Interface and implement the Method **findByFirstName**

```java
@Repository
public interface StudentRepository extends JpaRepository<Student,Long> {

    public List<Student> findByFirstName(String firstName);
}
```
+ Implement the method in the *StudentRepositoryTest*-Class

```java
@Test
public void printStudentByFirstName(){
        List<Student> students = studentRepository.findByFirstName("philipp");
        System.out.println("Studenten: "+students);
    }
```
![FindByFirstName](images/2021-10-30_FindByFirstName.jpg)

+ Change in the *StudentRepository*-Interface and implement the Method **findByFirstNameContaining**

```java
List<Student> findByFirstNameContaining(String name);
```
+ Implement the method in the *StudentRepositoryTest*-Class

```java
@Test
public void printStudentByFirstNameContaining(){
        List<Student> students = studentRepository.findByFirstNameContaining("ph");
        System.out.println("Studenten: "+students);
    }
```

![ContainsFirstName](images/2021-10-30_ContainsFirstName.jpg)

+ Change in the *StudentRepository*-Interface and implement more Methods

```java
List<Student> findByLastNameNotNull();

List<Student> findByGuardianName(String guardianName);

Student findByFirstNameAndLastName(String firstName, String lastName);
```
+ Implement the method in the *StudentRepositoryTest*-Class

```java
@Test
public void printStudentBasedOnGuardianName(){
        List<Student> students = studentRepository.findByGuardianName("Nikhil");
        System.out.println("Studenten: "+students);
    }
```
![FindByGuardianName](images/2021-10-30_FindByGuardianName.jpg)

**Spring Data JPA Documentation**

https://docs.spring.io/spring-data/jpa/docs/2.5.6/reference/html/#jpa.query-methods

![Spring-Data-Keywords](images/2021-10-20_JPA_Doc.jpg)

#### JPA @Query Annotation

+ Change in the *StudentRepository*-Interface and implement the Method **getStudentByEmailAddress**

```java
//JPQL-Query
@Query("select s from Student s where s.emailId = ?1")
Student getStudentByEmailAddress(String emailId;
```
+ Implement the method in the *StudentRepositoryTest*-Class

```java
@Test
public void printGetStudentByEmailAddress(){
        Student student = studentRepository
                .getStudentByEmailAddress("tomislav@gmail.com");
        System.out.println("Student: "+student);
    }
```
![GetStudentByEmailAddress](images/2021-10-30_GetStudentByEmail.jpg)

+ Change in the *StudentRepository*-Interface and implement the Method **getStudentFirstNameByEmailAddress**

```java
@Query("select s.firstName from Student s where s.emailId = ?1")
String getStudentFirstNameByEmailAddress(String emailId);
```
+ Implement the method in the *StudentRepositoryTest*-Class

```java
    @Test
    public void printGetStudentFirstNameByEmailAddress(){
        String firstName = studentRepository
                .getStudentFirstNameByEmailAddress("mr-phil@gmail.com");
        System.out.println("Firstname: "+firstName);
    }
```

![GetStudentFirstNameByEmail](images/2021-10-30_GetStudentFirstNameByEmail.jpg)

#### Native Queries Example

+ Change in the *StudentRepository*-Interface and implement the Method **getStudentByEmailAddressNative**

```java
    @Query(
            value = "select * from tbl_student s where s.email_address = ?1",
            nativeQuery = true
    )
    Student getStudentByEmailAddressNative(String emailId);
```
+ Implement the method in the *StudentRepositoryTest*-Class

```java
    @Test
    public void printGetStudentByEmailAddressNative(){
        Student student = studentRepository
                .getStudentByEmailAddressNative("mr-phil@gmail.com");
        System.out.println("Student: "+student);
    }
```

![GetStudentByEmailNative](images/2021-10-30_GetStudentByEmailNative.jpg)

Merke:

    You can use both, JPQL-Query or Native-Query

#### Query Named Params

+ Change in the *StudentRepository*-Interface and implement the Method **getStudentByEmailAddressNativeNamedParam**

```java
    @Query(
            value = "select * from tbl_student s where s.email_address = :emailId",
            nativeQuery = true
    )
    Student getStudentByEmailAddressNativeNamedParam(@Param("emailId") String emailId);
```
+ Implement the method in the *StudentRepositoryTest*-Class

```java
    @Test
    public void printGetStudentByEmailAddressNativeNamedParam(){
        Student student = studentRepository
                .getStudentByEmailAddressNativeNamedParam("mr-phil@gmail.com");
        System.out.println("Student: "+student);
    }
```
![NamedParam](images/2021-10-30_NamedParam.jpg)

#### @Transactional and @Modifying Annotation

+ Change in the *StudentRepository*-Interface and implement the Method **updateStudentNameByEmailId**

```java
    @Modifying
    @Transactional
    @Query(
            value = "update tbl_student set first_name = ?1 where email_address = ?2",
            nativeQuery = true
    )
    int updateStudentNameByEmailId(String firstName, String emailId);
```
+ Implement the method in the *StudentRepositoryTest*-Class

```java
    @Test
    public void updateStudentNameByEmailIdTest(){
        studentRepository.updateStudentNameByEmailId(
                "Thomas Update",
                "tomislav@gmail.com");
    }
```

![UpdateName](images/2021-10-30_UpdateName.jpg)

#### JPA One to One Relationship

+ Create the **Course** and the **CourseMaterial** Class in the *entity* package

**Course**

```java
package com.itkolleg.spring.data.jpa.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Course {

    @Id
    @SequenceGenerator(
            name = "course_sequence",
            sequenceName = "course_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "course_sequence"
    )
    private Long courseId;
    private String title;
    private Integer credit;
}
```

**CourseMaterial**

```java
package com.itkolleg.spring.data.jpa.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourseMaterial {

    @Id
    @SequenceGenerator(
            name = "course_material_sequence",
            sequenceName = "course_material_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "course_material_sequence"
    )
    private Long courseMaterialId;
    private String url;

    @OneToOne
    @JoinColumn(
            name = "course_id",
            referencedColumnName = "courseId"
    )
    private Course course;
}
```

![CourseTables](images/2021-10-30_CourseTables.jpg)

+ Create the **CourseRepository** Interface and the **CourseMaterialRepository** Interface in the *repository* package

```java
package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course,Long> {
}
```
```java
package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.CourseMaterial;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseMaterialRepository extends JpaRepository<CourseMaterial,Long> {
}
```
+ Generate the Test Class for the **CourseMaterialRepository**

```java
package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.Course;
import com.itkolleg.spring.data.jpa.tutorial.entity.CourseMaterial;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CourseMaterialRepositoryTest {

    @Autowired
    private CourseMaterialRepository repository;

    @Test
    public void saveCourseMaterial(){

        Course course = Course.builder()
                .title("DSA")
                .credit(6)
                .build();

        CourseMaterial courseMaterial = CourseMaterial.builder()
                .url("www.google.com")
                .course(course)
                .build();

        repository.save(courseMaterial);
    }
}
```

![FehlerCourseMaterial](images/2021-10-30_FehlerCourseMaterial.jpg)

Merke:

    Before we create a CourseMaterial, we have to create a Course. No CourseMaterial without Course!

#### Cascade Types

+ Change to the **CourseMaterial** Class and implement **cascading**

```java
    @OneToOne(
            cascade = CascadeType.ALL
    )
    @JoinColumn(
            name = "course_id",
            referencedColumnName = "courseId"
    )
    private Course course;
```
+ Test the Class **CourseMaterialRepositoryTest** again

![CascadeCourse](images/2021-10-30_CascadeCourse.jpg)

#### Fetch Types

There are two types of fetching:

+ **EAGER** (This strategy is a requirement on the persistence provider runtime that data must be eagerly fetched)
+ **LAZY** (The LAZY strategy is a hint to the persistence provider runtime that data should be fetched lazily when it is first accessed)

+ Add the fetch type in the **CourseMaterial** Class

```java
    @OneToOne(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
```
+ Implement the Method *printAllCourseMaterial* in the **CourseMaterialRepository**

```java
    @Test
    public void printAllCourseMaterial(){
        List<CourseMaterial> courseMaterials = repository.findAll();
        System.out.println("Coursematerials: "+ courseMaterials);
    }
```

![FehlerCourseMaterial2](images/2021-10-30_FehlerCourseMaterial2.jpg)

+ Change in the **CourseMaterial** Class and remove the **ToString-Method** for now

```java
@Builder
@ToString(exclude = "course")
public class CourseMaterial {
```
+ Test the **printAllCourseMaterial** Method in the **CourseMaterialRepositoryTest** Class again

![PrintAllCourseMaterial](images/2021-10-30_PrintAllCourseMaterials.jpg)

#### Uni & Bi directional Relationship

+ Change in the *CourseRepository* Interface and generate the Test **CourseRepositoryTest**

```java
package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.Course;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class CourseRepositoryTest {

    @Autowired
    private CourseRepository courseRepository;

    @Test
    public void printCourses(){
        List<Course> courses = courseRepository.findAll();

        System.out.println("Courses :" +courses);
    }
}
```
![PrintCourses](images/2021-10-30_PrintCourses.jpg)

+ Implement the Reference for bidirectional mapping in the **Course** Class

```java
    @OneToOne(
            mappedBy = "course"
    )
    private CourseMaterial courseMaterial;
```

+ Test the **printCourses** Method in the *CourseRepositoryTest* Class again

![PrintCourses2](images/2021-10-30_PrintCourses2.jpg)

#### JPA One to Many Relationship

+ Create the **Teacher** Class in the package *entity*

```java
package com.itkolleg.spring.data.jpa.tutorial.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Teacher {

    @Id
    @SequenceGenerator(
            name = "teacher_sequence",
            sequenceName = "teacher_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "teacher_sequence"
    )
    private Long teacherId;
    private String firstName;
    private String lastName;

    @OneToMany(
            cascade = CascadeType.ALL
    )
    @JoinColumn(
            name ="teacher_id",
            referencedColumnName = "teacherId"
    )
    private List<Course> courses;
}
```
+ Run the application and check the Database

![Teacher-Class](images/2021-10-30_Teacher.jpg)

+ Create the Interface **TeacherRepository** in the package *repository*

```java
package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher,Long> {
}
```
+ Generate the Test Class **TeacherRepositoryTest**

```java
package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.Course;
import com.itkolleg.spring.data.jpa.tutorial.entity.Teacher;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
class TeacherRepositoryTest {

    @Autowired
    private TeacherRepository teacherRepository;

    @Test
    public void saveTeacher(){
        Course coursePOS = Course.builder()
                .title("POS")
                .credit(8)
                .build();

        Course courseFSE = Course.builder()
                .title("FSE")
                .credit(8)
                .build();

        Teacher teacher = Teacher.builder()
                .firstName("Claudio")
                .lastName("Landerer")
                .courses(List.of(coursePOS,courseFSE))
                .build();

        teacherRepository.save(teacher);
    }
}
```

![Teacher](images/2021-10-30_Teacher2.jpg)

+ With this example you can see that CourseMaterial is **optional**. We want to change this and make CourseMaterial required if you create a new Course. Change to the *CourseMaterial* Class and implement that.

```java
    @OneToOne(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            // Now CourseMaterial is required for every Course
            optional = false
    )
    @JoinColumn(
            name = "course_id",
            referencedColumnName = "courseId"
    )
    private Course course;
```
![OptionalFalse](images/2021-10-30_OptionalFalse.jpg)

#### JPA Many to One Relationship

+ Comment out the @OneToMany Annotation in the **Teacher** Class

+ Change in the *Course* Class and implement the Reference of **Teacher** with ManyToOne

```java
    @ManyToOne(
            cascade = CascadeType.ALL
    )
    @JoinColumn(
            name = "teacher_id",
            referencedColumnName = "teacherId"
    )
    private Teacher teacher;
```

+ Change in the *CourseRepositoryTest* Class and implement the Method **saveCourseWithTeacher**

```java
    @Test
    public void saveCourseWithTeacher(){
        Teacher teacher = Teacher.builder()
                .firstName("Melanie")
                .lastName("Osl")
                .build();

        Course course = Course.builder()
                .title("PHP")
                .credit(6)
                .teacher(teacher)
                .build();

        courseRepository.save(course);
    }
```

![ManyToOne](images/2021-10-30_ManyToOne.jpg)

#### Paging and Sorting

The **JpaRepository** extends from the **PagingAndSortingRepository**

+ Change to the *CourseRepositoryTest* Class and implement the Method **findAllPagination**

```java
    @Test
    public void findAllPagination(){
        Pageable firstPageWithThreeRecords = PageRequest.of(0,3);
        Pageable secondPageWithTwoRecords = PageRequest.of(1,2);

        List<Course> courses = courseRepository.findAll(firstPageWithThreeRecords).getContent();
        long totalElements = courseRepository.findAll(firstPageWithThreeRecords).getTotalElements();
        long totalPages = courseRepository.findAll(firstPageWithThreeRecords).getTotalPages();

        System.out.println("Total Pages: " +totalPages);
        System.out.println("Total Elements: " +totalElements);
        System.out.println("Courses: " +courses);
    }
```

+ Test the Method with **firstPageWithThreeRecords**

![firstPageWithThreeRecords](images/2021-10-30_firstPageWithThreeRecords.jpg)

+ Test the Method with **secondPageWithTwoRecords**

![secondPageWithTwoRecords](images/2021-10-30_secondPageWithTwoRecords.jpg)

+ Implement Method Pagination with Sorting (findAllSorting) in the *CourseRepositoryTest* Class

```java
    @Test
    public void findAllSorting(){
        Pageable sortByTitle = PageRequest.of(0,2, Sort.by("title"));
        Pageable sortByCreditDesc = PageRequest.of(0,2,Sort.by("credit").descending());
        Pageable sortByTitleAndCreditDesc = PageRequest.of(0,2,Sort.by("title").descending().and(Sort.by("credit")));

        List<Course> courses = courseRepository.findAll(sortByTitle).getContent();

        System.out.println("Courses: " +courses);
    }
```

![findAllSorting](images/2021-10-30_findAllSorting.jpg)

+ Change in the *CourseRepository* Interface and implement the Method **findByTitleContaining**

```java
package com.itkolleg.spring.data.jpa.tutorial.repository;

import com.itkolleg.spring.data.jpa.tutorial.entity.Course;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends JpaRepository<Course,Long> {

    Page<Course> findByTitleContaining(String title, Pageable pageable);
}
```
+ Change in the *CourseRepositoryTest* Class and implement the Test Method **printFindByTitleContaining**

```java
    @Test
    public void printFindByTitleContaining(){
        Pageable firstPageTenRecords = PageRequest.of(0,10);

        List<Course> courses = courseRepository.findByTitleContaining("D",firstPageTenRecords).getContent();

        System.out.println("Courses: " +courses);
    }
```
![FindByTitleContaining](images/2021-10-30_FindByTitleContaining.jpg)

#### JPA Many to Many Relationship

**For Many to Many Relationship you need a third Table which represents both IDs (Student & Course). For this you can use the @JoinTable Annotation**

+ Change in the *Course* Class and implement the Many to Many Reference for **Student**

```java
    @ManyToMany(
            cascade = CascadeType.ALL
    )
    @JoinTable(
            name = "student_course_mapping",
            joinColumns = @JoinColumn(
                    name = "course_id",
                    referencedColumnName = "courseId"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "student_id",
                    referencedColumnName = "studentId"
            )
    )
    private List<Student> students;

    public void addStudents(Student student){
        if(students == null) students = new ArrayList<>();
            students.add(student);
    }
```

+ Run the application and check out the Database

![ManyToMany](images/2021-10-30_ManyToMany.jpg)

+ Change to the *CourseRepositoryTest* and implement the Method **saveCourseWithStudentAndTeacher**

```java
    @Test
    public void saveCourseWithStudentAndTeacher(){
        Teacher teacher = Teacher.builder()
                .firstName("Stefan")
                .lastName("Stolz")
                .build();

        Student student = Student.builder()
                .firstName("Manuel")
                .lastName("Forsthuber")
                .emailId("manuel@gmail.com")
                .build();

        Course course = Course.builder()
                .title("AI")
                .credit(12)
                .teacher(teacher)
                .build();

        course.addStudents(student);

        courseRepository.save(course);
    }
```
![ManyToMany2](images/2021-10-30_ManyToMany2.jpg)

#### DTO (Datentransferobjekt oder Transferobjekt)

Entwurfsmuster aus dem Bereich der Softwareentwicklung. Es bündelt mehrere Daten in einem Objekt, sodass sie durch einen einzigen Programmaufruf übertragen werden können. Transferobjekte werden in verteilten Systemen eingesetzt, um mehrere zeitintensive Fernzugriffe durch einen einzigen zu ersetzen. 

## WEITERE ENTITÄT HINZUFÜGEN

### Erweiterung  

Tutorial 2: SPRING BOOT FULL STACK DEPARTMENTMANAGEMENT

https://www.youtube.com/watch?v=c3gKseNAs9w

Entität: **Employee**

### Vorgehensweise
#### Implementierung der Entität Employee

![Employee](images/2021-11-13_04_Employee1.jpg)

```java
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long employeeId;
    @NotBlank(message = "Please Add Employee Firstname!")
    private String employeeFirstName;
    @NotBlank(message = "Please Add Employee Lastname!")
    private String employeeLastName;

    //Referenzierung zur Entität Department
    @ManyToOne
    private Department department;
}
```
#### Bearbeitung der Entität Department

```java
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long departmentId;
    @NotBlank(message = "Please Add Department Name!")
    private String departmentName;
    private String departmentAddress;
    private String departmentCode;

    //Referenzierung zur Entität Employee
    @OneToMany(mappedBy = "department")
    private List<Employee> employees;
}
```

#### EmployeeController Klasse

```java
@RestController
public class EmployeeController implements EmployeeService {

    @Autowired
    private EmployeeService employeeService;

    public final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @PostMapping("/employee")
    public Employee saveEmployee(@Valid @RequestBody Employee employee){
        LOGGER.info("Inside saveEmployee of EmployeeController");
        return employeeService.saveEmployee(employee);
    }

    @GetMapping("/employee")
    public List<Employee> fetchEmployeeList(){
        LOGGER.info("Inside fetchEmployeeList of EmployeeController");
        return employeeService.fetchEmployeeList();
    }

    @GetMapping("/employee/{id}")
    public Employee fetchEmployeeById(@PathVariable("id")Long employeeId) throws EmployeeNotFoundException {
        return employeeService.fetchEmployeeById(employeeId);
    }

    @DeleteMapping("/employee/{id}")
    public String deleteEmployeeById(@PathVariable("id") Long employeeId){
        employeeService.deleteEmployeeById(employeeId);
        return "Employee deleted Successfully!";
    }

    @PutMapping("/employee/{id}")
    public Employee updateEmployee(@PathVariable("id") Long employeeId, @RequestBody Employee employee){
        return employeeService.updateEmployee(employeeId, employee);
    }

    @GetMapping("/employee/name/{name}")
    public Employee fetchEmployeeByName(@PathVariable("name") String employeeLastName){
        return employeeService.fetchEmployeeByName(employeeLastName);
    }
}
```
#### EmployeeService Interface

```java
public interface EmployeeService {

    Employee saveEmployee(Employee employee);

    List<Employee> fetchEmployeeList();

    Employee fetchEmployeeById(Long employeeId) throws EmployeeNotFoundException;

    String deleteEmployeeById(Long employeeId);

    Employee updateEmployee(Long employeeId, Employee employee);

    Employee fetchEmployeeByName(String employeeLastName);
}
```
#### EmployeeServiceImplementation Klasse

```java
@Service
public class EmployeeServiceImplementation implements EmployeeService{

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Employee saveEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> fetchEmployeeList() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee fetchEmployeeById(Long employeeId) throws EmployeeNotFoundException {
        Optional<Employee> employee = employeeRepository.findById(employeeId);
        if(!employee.isPresent()){
            throw new EmployeeNotFoundException("Employee Not Available!");
        }else{
            return employee.get();
        }
    }

    @Override
    public void deleteEmployeeById(Long employeeId) {
        employeeRepository.deleteById(employeeId);
    }

    @Override
    public Employee updateEmployee(Long employeeId, Employee employee) throws EmployeeNotFoundException {
        Optional<Employee> employeeDatabase = employeeRepository.findById(employeeId);
        if (!employeeDatabase.isPresent()) {
            throw new EmployeeNotFoundException("Employee Not Available!");
        }
        /**
         * Weitere Möglichkeit / Schreibweise
         * Employee employee = employeeDatabase.get();
         */
        if (Objects.nonNull(employee.getEmployeeFirstName()) && !"".equalsIgnoreCase(employee.getEmployeeFirstName())) {
            employeeDatabase.get().setEmployeeFirstName(employee.getEmployeeFirstName());
        }
        if (Objects.nonNull(employee.getEmployeeLastName()) && !"".equalsIgnoreCase(employee.getEmployeeLastName())) {
            employeeDatabase.get().setEmployeeFirstName(employee.getEmployeeLastName());
        }
        return employeeRepository.save(employeeDatabase.get());
    }
}
```
#### Implementierung der Test-Klassen

##### EmployeeControllerTest

```java
@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EmployeeService employeeService;
    private Employee employee;

    @BeforeEach
    void setUp(){
        employee = Employee.builder()
                .employeeFirstName("Thomas")
                .employeeLastName("Kutz")
                .employeeId(1L)
                .build();
    }
```

+ Test **saveEmployee**

```java
    @Test
    void saveEmployee() throws Exception {
        Employee inputEmployee = Employee.builder()
                .employeeFirstName("Thomas")
                .employeeLastName("Kutz")
                .build();

        Mockito.when(employeeService.saveEmployee(inputEmployee))
                .thenReturn(employee);

        //POST-Operation
        mockMvc.perform(post("/employee")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"employeeFirstName\":\"Thomas\",\n" +
                                "\t\"employeeLastName\":\"Kutz\",\n" +
                                "}"))
                .andExpect(status().isOk());
    }
```

![Test-SafeEmployee](images/2021-11-14_Test-SafeEmployee.jpg)

+ Test **fetchEmployeeById**

```java
    @Test
    void fetchEmployeeById() throws Exception {
        Mockito.when(employeeService.fetchEmployeeById(1L))
                .thenReturn(employee);
        //GET-Operation
        mockMvc.perform(get("/employee/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.employeeLastName").value(employee.getEmployeeLastName()));
    }
```
![Test-fetchEmployeeById](images/2021-11-14_Test_fetEmployeeById.jpg)

##### EmployeeRepositoryTest

```java
public class EmployeeRepositoryTest {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void setUp() {
        Employee employee = Employee.builder()
                .employeeFirstName("Thomas")
                .employeeLastName("Kutz")
                .build();

        entityManager.persist(employee);
    }
```

+ Test **whenFindById_thenReturnEmployee**

```java
    @Test
    public void whenFindById_thenReturnEmployee(){
        Employee employee = employeeRepository.findById(1L).get();
        assertEquals(employee.getEmployeeLastName(), "Kutz");
    }
```
![Test-RepositoryFindById](images/2021-11-14_Test-RepositoryFindById.jpg)

##### EmployeeServiceTest

```java
@SpringBootTest
public class EmployeeServiceTest {

    @Autowired
    private EmployeeService employeeService;
    @MockBean
    private EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        Employee employee = Employee.builder()
                .employeeFirstName("Thomas")
                .employeeLastName("Kutz")
                .employeeId(1L)
                .build();

        Mockito.when(employeeRepository.findByEmployeeLastNameIgnoreCase("Kutz"))
                .thenReturn(employee);
    }
```
+ Test **whenValidEmployeeName_thenEmployeeShouldFound**

```java
    @Test
    @DisplayName("Get Data based Valid Employee Name")
    public void whenValidEmployeeName_thenEmployeeShouldFound(){
        String employeeLastName = "Kutz";
        Employee found = employeeService.fetchEmployeeByLastName(employeeLastName);
        assertEquals(employeeLastName, found.getEmployeeLastName());
    }
```
![Test-Service](images/2021-11-14_Test-Service.jpg)

## SpringBoot Exercises
### Beispiel Car

Anforderungen

+ 1 Domänenklasse mit 3 Datenfeldern
+ REST-Controller mit API für CRUD
+ ControllerLayer - ServiceLayer - Repo
+ Exceptions verwenden

Domäne: **Car**

Datenfelder:

+ brand
+ model
+ yearOfConstruction

**Documentation**

+ Project Structure

![Struktur-Car](images/2021-11-14_Struktur-Car.jpg)

+ Implementierung **CarControllerTest**

![Save-Car](images/2021-11-14_Save-Car.jpg)

+ Post-Request

![Post-Request-Car](images/2021-11-14_Post_Request_Car.jpg)

### Beispiel Library

Anforderungen

+ 1 Domänenklasse mit 3 Datenfeldern
+ REST-Controller mit API für CRUD
+ ControllerLayer - ServiceLayer - Repo
+ Exceptions verwenden

Domäne: **Library**

Datenfelder:

+ author
+ title
+ pages
+ bookAvailable

### Beispiel Wetterstation

Anforderungen

+ 1 Domänenklasse mit 3 Datenfeldern
+ REST-Controller mit API für CRUD
+ ControllerLayer - ServiceLayer - Repo
+ Exceptions verwenden

Domäne: **Messwerte**

Datenfelder:

+ name
+ value